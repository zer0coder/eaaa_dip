var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
/////////////////////////////////////////////////////////////////////
app.set('port', 8080);
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(morgan('tiny'));
/////////////////////////////////////////////////////////////////////
var client_num = 1;
var kunder = [];

var hbs = require('hbs');
hbs.registerPartials('templates');

app.set('view engine', 'hbs');
app.set('views', 'templates');
/////////////////////////////////////////////////////////////////////
app.get('/', function (req, res) {

    //// NO HBS - Lavet uden for given tid for prøve-eksamen!
    // var html = "<ul>";
    // for(var i = 0; i<kunder.length; i++) {
    //     html += "<li><a href=\"\\" + kunder[i].id + "\"> Name: " + kunder[i].name + "</a></li>";
    // }
    // html += "</ul>";
    //
    // res.set("Content-Type", "text/html");
    // res.writeHead(200);
    // res.write(html);
    // res.end();

    //// HBS - Lavet inden for den givet tid for prøve-eksamen!
    res.render('kunde',
        {
            "kunde": kunder
        });

});

app.get("/:id", function (req, res) {

    var id = req.params.id;

    var kunde = kunder[id-1];

    res.json(kunde);
});

/////////////////////////////////////////////////////////////////////
app.listen(8080);
console.log("Listening on port 8080 ...");
/////////////////////////////////////////////////////////////////////
var random_client = function () {

    var name = "Klient " + client_num;

    var address = "Test Vej 100"+client_num;
    var tlf = 1000000+client_num;
    client_num++;

    return {
        name: name,
        adress: address,
        tlf: tlf,
        id: client_num-1
    };

};

for(var i = 0; i<50; i++) {
    kunder.push(random_client());
}
