var json_text = "[{ \"name\": \"Tom\", \"throws\":[1, 2, 3]}, { \"name\": \"John\", \"throws\":[4, 5, 6]}]";

var kast = JSON.parse(json_text);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var dice = function (player_name) {

    var players = kast;

    for(var i = 0; i<players.length; i++) {

        if(players[i].name === player_name) {

            return players[i].throws;
        }

    }

    return "Player: " + player_name + ", does not exist!";
};

var getPlayer = function (name) {
    return dice(kast, name);
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var max_throw = function () {

    var max = -1;
    var arr = [];

    var players = kast;

    for(var i = 0; i<players.length; i++) {

        var throws = players[i].throws;

        for(var l = 0; l<throws.length; l++) {

            if(max < throws[l])
                max = throws[l]

        }

        arr.push(max);

    }

    return arr;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
console.log(dice("Tom"));
console.log(dice("John"));
console.log(dice("Ian"));
console.log();
console.log(max_throw());