$(function () {

    var url = "https://dip-chat-server.herokuapp.com/api/";

    var chatArea = $("#chatArea");

    $.get(url+"messages", function (data) {

        if(data.length > 0) {

            var text = chatArea.text();
            data.forEach(function (entry) {

                text += entry.name + ": " + entry.text + "\n";

            });

            chatArea.text(text);

        }

    });

    $("#chatForm").on("submit", function (event) {
        event.preventDefault();
        var txt = $("#inputText");

        $.post(url+"messages", {
            name: "Eks18Opg12",
            roomName: "messages",
            text: txt.val()
        }).then(function (response) {
            console.log(response);
            txt.val("");
        }).catch(function (reason) {
            console.log("Indsend fejl: \n" + JSON.stringify(reason));
        });
    });

});