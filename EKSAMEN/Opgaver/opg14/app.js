var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
/////////////////////////////////////////////////////////////////////
app.set('port', 8080);
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(morgan('tiny'));
app.use(express.static("public"));
/////////////////////////////////////////////////////////////////////
var messages = [];
var global_id = 0;
/////////////////////////////////////////////////////////////////////
app.get("/", function (req, res) {

    res.send("Hello world");

});

app.get("/messages", function (req, res) {

    res.json(messages);

});

app.get("/messages/:id", function (req, res) {

    var id = req.params.id;

    res.json(getMessage(id));

});

app.post("/messages", function (req, res) {

    var text = req.body.message;

    console.log(text);

    if(text.length >= 0 && text.length !== " ") {

        pushMessage(text, global_id++);
        res.status(304).send("Created message!");

    } else {
        res.status(403).send("Error!");
    }

});

app.delete("/messages/:id", function (req, res) {

    var id = req.params.id;

    if(deleteMessage(id)) {
        res.send("Deleted message!");
    } else {
        res.status(403).send("ID does not exist!");
    }

});

app.put("/messages/:id", function (req, res) {

    var id = req.params.id;
    var text = req.body.message;

    if(updateMessage(text, id)) {
        res.send("Message updated!");
    } else {
        res.status(403).send("ID does not exist!");
    }

});
/////////////////////////////////////////////////////////////////////
app.listen(8080);
console.log("Listening on port 8080 ...");
/////////////////////////////////////////////////////////////////////
var pushMessage = function (text, num) {

    messages.push({
        message: text,
        id: num
    })

};

var getMessage = function(id) {

    return messages.filter(function (msg) {
        return msg.id == id;
    });

};

var deleteMessage = function(id) {

    for(var i = 0; i<messages.length; i++) {
        if(messages[i].id == id) {
            delete messages[i];
            return true
        }
    }

    return false;
};

var updateMessage = function(text, id) {

    for(var i = 0; i<messages.length; i++) {
        if(messages[i].id == id) {
            messages[i] = text;
            return true
        }
    }

    return false;
};
/////////////////////////////////////////////////////////////////////
pushMessage("Hello, World", 42);
