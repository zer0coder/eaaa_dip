$(function () {

    /////////////////////////////////////////////////////////////////////

    var textArea = $("#chatArea");

    var url = "http://localhost:8080/";


    /////////////////////////////////////////////////////////////////////




    /////////////////////////////////////////////////////////////////////

    $("#sendText").on("click", function (event) {
       event.preventDefault();

       var text = $("#inputText").val();

       $.post(url+"messages", {
           message: text
       }, function (data) {

           console.log(data);

           updateChatArea();

       });

    });

    /////////////////////////////////////////////////////////////////////

    var updateChatArea = function () {

        $.get(url+"messages", function (result) {

            textArea.val("");

            result.forEach(function (message) {

                var curr = textArea.val();

                curr += "(" + message.id + ")" + message.message + "\n";

                textArea.val(curr);
            });

        });

    };

    /////////////////////////////////////////////////////////////////////

    updateChatArea();
});