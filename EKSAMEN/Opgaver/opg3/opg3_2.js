
var fib = function (n) {

    if(n == 0 || n == 1) {
        return n;
    } else {
        return fib(n-2)+fib(n-1);
    }

};

var max = function (array) {

    var curr = array[0];

    for(var i = 0; i<array.length; i++) {

        if(array[i] >= curr)
            curr = array[i];

    }

    return curr;

};

var contains = function (array, element) {

    for(var i = 0; i<array.length; i++) {

        if (array[i] === element)
            return true;
    }

    return false;
};

var sum = function (array) {

    return array.reduce(function (x, y) {
        return x+y;
    });
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////
console.log(fib(10));

var arr = [10, 5, 9, 11, 14, 5, 15, 7];
var arr2 = ["a", "b", "e", "m", "c"];

console.log(max(arr));
console.log(max(arr2));

console.log(contains(arr, 11));
console.log(contains(arr, 16));
console.log(contains(arr, "k"));

console.log(contains(arr2, "b"));
console.log(contains(arr2, "k"));

console.log(sum(arr));
console.log(sum(arr2));
