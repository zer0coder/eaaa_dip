var compare = function (s1, s2) {

    for(var i = 0; i<s1.length; i++) {
        for(var j = 0; j<s2.length; j++) {
            if(s1[i] < s2[j])
                return -1;
            else if (s1[i] > s2[j])
                return 1;
        }
    }
    return 0;


};

var compareLen = function (s1, s2) {

    if(s1.length < s2.length)
        return -1;
    else if (s1.length > s2.length)
        return 1;
    else
        return 0;
};

var compareIgnoreCase = function (a, b) {

    var s1 = a.toUpperCase();
    var s2 = b.toUpperCase();

    for(var i = 0; i<s1.length; i++) {
        for(var j = 0; j<s2.length; j++) {
            if(s1[i] < s2[j])
                return -1;
            else if (s1[i] > s2[j])
                return 1;
        }
    }
    return 0;

};

var array = ["IJklM", "GH", "EFg", "CD", "AB"];
var array2 = ["ijklM", "GH", "efg", "CD", "ab"];
var array3 = ["ijKlM", "GH", "efg", "CD", "ab"];

console.log(array.sort(compare));
console.log(array2.sort(compare));
console.log(array3.sort(compare));

 array = ["IJklM", "GH", "EFg", "CD", "AB"];
 array2 = ["ijklM", "GH", "efg", "CD", "ab"];
 array3 = ["ijKlM", "GH", "efg", "CD", "ab"];

console.log(array.sort(compareLen));
console.log(array2.sort(compareLen));
console.log(array3.sort(compareLen));

 array = ["IJklM", "GH", "EFg", "CD", "AB"];
 array2 = ["ijklM", "GH", "efg", "CD", "ab"];
 array3 = ["ijKlM", "GH", "efg", "CD", "ab"];

console.log(array.sort(compareIgnoreCase));
console.log(array2.sort(compareIgnoreCase));
console.log(array3.sort(compareIgnoreCase));