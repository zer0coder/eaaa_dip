$(function () {


    $("ul li").each(function () {

        if($(this).text() < 0) {
            $(this).addClass("red");
        }

    });

    var list = $("ul");
    var sumText = $("#sum");

    var sum_numbers = function () {

        var items = list.find("li");
        var total = 0;

        for(var i = 0; i<items.length; i++) {

            var num = Number.parseFloat($(items[i]).text());

            total += num;
        }

        sumText.text(total);

    };

    sum_numbers();

    $("li").on("click", function () {

        $(this).remove();
        sum_numbers();

    })

});