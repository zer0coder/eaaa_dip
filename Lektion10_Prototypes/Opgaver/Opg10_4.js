function StringStack() {
    var stack = [];
    var index = 1;
    this.push = function(element) {
        if(typeof element === "string") {
            stack[index++] = element;
        } else {
            throw "Not a string!";
        }
    };

    this.pop = function() {
        var res = stack[0];
        var newStack = [];
        for(var i = 1; i<stack.length; i++) {
            newStack[i-1] = stack[i];
        }
        stack = newStack;
        index--;
        return res;
    };
}

var stack = new StringStack();
console.log(stack.pop());
stack.push("A");
stack.push("B");
stack.push("C");
stack.push("D");

console.log(stack.pop());
console.log(stack.pop());

stack.push("E");
console.log(stack.pop());
console.log(stack.pop());
stack.push("F");
stack.push("G");
console.log(stack.pop());
console.log(stack.pop());
console.log(stack.pop());
console.log(stack.pop());
