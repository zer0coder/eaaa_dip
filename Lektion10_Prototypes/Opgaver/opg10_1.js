

function Person(navn, alder, cpr) {
    this.nam = navn;
    this.age = alder;
    this.cpr = cpr;
}

Person.prototype.toString = function() {
    return this.nam + ", " + this.age + ", " + this.cpr;
};

Person.prototype.valueOf = function() {
    return this.cpr;
};

Person.prototype.equals = function(p) {
    return p.constructor === Person && p.valueOf() === this.valueOf();
};

Person.compare = function (a, b) {
    var s1 = a.toString();
    var s2 = b.toString();
    if (typeof s1 === "string" && typeof s2 === "string") {

        for(var i = 0; i<s1.length; i++) {
            for(var j = 0; j<s2.length; j++) {
                if(s1[i] < s2[j])
                    return -1;
                else if (s1[i] > s2[j])
                    return 1;
            }
        }
        return 0;

    } else {
        throw "Not the same types!";
    }
};

var anders = new Person("Anders", 25, 909090);
var thomas = new Person("Thomas", 31, 909091);
var lene = new Person("Lene", 45, 909092);

console.log(anders.toString());
console.log(anders.valueOf());
console.log(anders.equals(anders));
console.log(anders.equals(thomas));

var persons = [];
persons.push(thomas);
persons.push(lene);
persons.push(anders);

console.log();
console.log(persons);
console.log();
persons.sort(Person.compare);
console.log(persons);