$(document).ready(function () {
    var disabled = 0;
    $("#btnRoll").on("click", function () {

        if (throwCount < 3) {
            roll();
            // update diceContainer
            $("#d0").find("input").val(values[0]);
            $("#d1").find("input").val(values[1]);
            $("#d2").find("input").val(values[2]);
            $("#d3").find("input").val(values[3]);
            $("#d4").find("input").val(values[4]);
            $("#throw").text(throwCount);
            $("#turn").text(turn);

            // update scores
            $("table#resultTable").find("input").not("#inputSum").not("#inputBonus").not("#inputTotal").each(function (index) {
                $(this).val(totalScore[index])
            })
        }

    });

    // ny runde
    $("table#resultTable").find("input").not("#inputSum").not("#inputBonus").not("#inputTotal").on("click", function () {

        if (!$(this).prop("disabled") && throwCount !== 0) {
            if (disabled !== 14) {
                $(this).prop("disabled", true);
                disabled++;
            } else {
                disabled = 0;
                alert("Tillykke! Du gennemførte spillet med en score på: " + total.toString());
                $("#resultTable").find("input").not("#inputSum").not("#inputBonus").not("#inputTotal").prop("disabled", false);
            }

            // kald tilbage til logic at bruger har låst et felt, og at ny runde skal begynde

            newRound(parseInt($(this).attr('id')));

            $("table#dice").find("input").prop("disabled", false);

            $("#d0").find("input").val(values[0]);
            $("#d1").find("input").val(values[1]);
            $("#d2").find("input").val(values[2]);
            $("#d3").find("input").val(values[3]);
            $("#d4").find("input").val(values[4]);
            $("#throw").text(throwCount);
            $("#turn").text(turn);

            $(function () {
                $("#inputSum").val(sum);
                $("#inputBonus").val(bonus);

                $("#inputTotal").val(total)
            });

            $("table#resultTable").find("input").not("#inputSum").not("#inputBonus").not("#total").each(function (index) {
                $(this).val(totalScore[index])
            })
        }
    });

    $("table#dice").find("input").on("click", function () {
        if (throwCount !== 0) {
            if (!$(this).prop("disabled")) {
                $(this).prop("disabled", true);
            }

            var i = parseInt($(this).attr('id').charAt(0));
            lockDie(i);
        }
    });
});



