var values = [0, 0, 0, 0, 0];
var held = [false, false, false, false, false];
var totalScore = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var lockedScore = [false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false];
var throwCount = 0;
var turn = 1;

var sum = 0;
var bonus = 0;
var total = 0;

function roll() {
    for (var i = 0; i < 5; i++) {
        if (!held[i]) {
            values[i] = Math.floor(Math.random() * 6) + 1;
        }
    }

    updateScores();
    if (throwCount >= 3) {
        resetDice();
        turn++;
    } else
        throwCount++;
}

function lockDie(index) {
    held[index] = true
}

function unlockDie(index){
    held[index] = false;
}

function resetDice() {
    for (var i = 0; i < 5; i++) {
        values[i] = 0;
        held[i] = false;
    }
    throwCount = 0;
}

function newRound(LockElement) {
    lockedScore[LockElement] = true;
    held = [false, false, false, false, false];
    values = [0, 0, 0, 0, 0];
    turn += 1;
    throwCount = 0;

    for (var i = 0; i < 15; i++) {
        if (!lockedScore[i]) {
            totalScore[i] = 0;
        }
    }

    // calculate og total
    total = calcTotal();

    // calculate sum
    sum = 0;
    for (var j = 0; j <= lockedScore.length; j += 1) {
        if (lockedScore[j] === true) {
            sum += totalScore[j];
        }
    }

    if (sum >= 63) {
        bonus = 50
    }

    if (hasGameFinished()) {
        finishAndRestartGame();
    }
}

function hasGameFinished() {
    for (var k = 0; k < lockedScore.length; k += 1) {
        if (lockedScore[k] === false) {
            return false;
        }
    }
    return true;
}

function finishAndRestartGame() {
    values = [0, 0, 0, 0, 0];
    held = [false, false, false, false, false];
    totalScore = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    lockedScore = [false, false, false, false, false, false, false,
        false, false, false, false, false, false, false, false];
    throwCount = 0;
    turn = 1;
    sum = 0;
    bonus = 0;
    total = 0;
}

function calcTotal() {
    var result = 0;
    for (var i = 0; i < 15; i++) {
        if (lockedScore[i]) {
            result += totalScore[i];
        }
    }
    return result;
}

function updateScores() {
    var newScores = CalculateFields(values);

    for (var i = 0; i < 15; i++) {
        if (!lockedScore[i]) {
            totalScore[i] = newScores[i]
        }
    }
}


function CalculateFields(dice) {
    // for instance if you had 3 ones and 3 fives it would be [3,0,0,0,5,0]
    var numberOfRecurrencesDice = [0, 0, 0, 0, 0, 0];

    // kalkulere numberOfRecurrencesDice
    for (var i = 0; i < dice.length; i += 1) {
        numberOfRecurrencesDice[dice[i] - 1] += 1;
    }

    function getTopPointBlock() {
        // kalkulere den øverste block
        var oeverstPointBlok = [0, 0, 0, 0, 0, 0];

        for (var i = 0; i <= dice.length; i += 1) {
            oeverstPointBlok[i] = numberOfRecurrencesDice[i] * (i + 1);
        }
        return oeverstPointBlok;
    }

    function getOnePairTwoPair() {
        var onePair = 0;
        var twoPair = 0;

        for (var i = 5; i >= 0; i -= 1) {
            if (numberOfRecurrencesDice[i] >= 2) {
                if (onePair === 0) {
                    onePair = 2 * (i + 1);
                }
                else {
                    twoPair = 2 * (i + 1) + onePair;
                }
            }
        }
        return [onePair, twoPair]
    }


    function getThreeAndFourOfAKind() {
        var threeOfaKind = 0;
        var fourOfaKind = 0;

        for (var i = 5; i >= 0; i -= 1) {
            if (numberOfRecurrencesDice[i] >= 3 && threeOfaKind === 0) {
                threeOfaKind = (i + 1) * 3
            }
            if (numberOfRecurrencesDice[i] >= 4 && fourOfaKind === 0) {
                fourOfaKind = (i + 1) * 4
            }
        }
        return [threeOfaKind, fourOfaKind];
    }

    function getYatzy() {
        // yatzy (fem ens)
        var yatzy = 0;
        for (var i = 5; i >= 0; i -= 1) {
            if (numberOfRecurrencesDice[i] === 5) {
                yatzy = 50;
            }
        }
        return [yatzy];
    }

    function getSmallAndLargeStraight() {
        // kalkulerer lille straight stor straight.
        var smallStraight = 0;
        var largeStraight = 0;
        var diceSorted = dice.concat().sort(function sortNumber(a, b) {
            return a - b;
        });
        if (diceSorted[0] === 1 && diceSorted[1] === 2 && diceSorted[2] === 3
            && diceSorted[3] === 4 && diceSorted[4] === 5) {

            smallStraight = 15;
        }
        if (diceSorted[0] === 2 && diceSorted[1] === 3 && diceSorted[2] === 4
            && diceSorted[3] === 5 && diceSorted[4] === 6) {
            largeStraight = 20;
        }
        return [smallStraight, largeStraight]
    }

    function getFullHouse() {
        var fullHouse = 0;

        var tripleTemp = numberOfRecurrencesDice.indexOf(3);
        var doubleTemp = numberOfRecurrencesDice.indexOf(2);
        if (tripleTemp !== -1 && doubleTemp !== -1) {
            fullHouse = tripleTemp * 3 + doubleTemp * 2
        }
        return [fullHouse]
    }

    function getChance() {
        // Chance (alle øjne lægges sammen, højst 30)
        var chance = 0;

        for (var i = 0; i < dice.length; i += 1) {
            chance += dice[i];
        }
        return chance;
    }

    return [].concat.apply([], [getTopPointBlock(), getOnePairTwoPair(), getThreeAndFourOfAKind(),
        getSmallAndLargeStraight(), getFullHouse(), getChance(), getYatzy()]);
}
