function Person(name) {
    this.name = name;
    Person.number++
}
Person.number = 0;
Person.prototype.toString = function () {
    return this.name;
};

var person = new Person('NN');
console.log(Person.number); // => 1
console.log(person.toString()); // => NN

console.log(person.constructor === Person); // => true
console.log(person.constructor); // => { [Function: Person] number: 1 }
console.log(person.toString.toString());
//  =>  function () {
//          return this.name;
//      }

