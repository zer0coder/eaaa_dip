function Animal(name) {
    this.name = name;
}
Animal.prototype.toString = function() {
    return this.name;
};
///////////////////////////////////////////////////////////////////////////////////
function Cat(name, sound) {
    Animal.call(this, name);
    this.sound = sound;
}
Cat.prototype = Object.create(Animal.prototype);
Cat.constructor = Cat;

Cat.prototype.toString = function () {
    return Animal.prototype.toString.call(this) + " says " + this.sound;
};
///////////////////////////////////////////////////////////////////////////////////
function Dog(name, sound) {
    Animal.call(this, name);
    this.sound = sound;
}
Dog.prototype = Object.create(Animal.prototype);
Dog.constructor = Dog;

Dog.prototype.toString = function () {
    return Animal.prototype.toString.call(this) + " says " + this.sound;
};
///////////////////////////////////////////////////////////////////////////////////
var animals = [];
animals.push(new Cat("Garfield", "Meow"));
animals.push(new Dog("Snoopy", "Woof"));

animals.forEach(function (a){
    console.log(a.toString());
});
///////////////////////////////////////////////////////////////////////////////////







// ///////////////////////////////////////////////////////////////////////////////////
// function Cat(name) {
//     Animal.call(this, name);
// }
// Cat.prototype.sound = function () {
//     return "Meow";
// };
// ///////////////////////////////////////////////////////////////////////////////////
// function Dog(name) {
//     this.name = name;
// }
// Dog.prototype.sound = function () {
//     return "Woof";
// };
// ///////////////////////////////////////////////////////////////////////////////////