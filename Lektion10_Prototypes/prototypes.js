// var p = {x: 1};
// var o = Object.create(p);
// o.y = 2;
// Object.prototype.z = 3;
// console.log(o.x); // => 1
// console.log(o.y); // => 2
// console.log(o.z); // => 3
// o.x = 4;
// console.log(o.x); // => 4
// console.log(p.x); // => 1

var f = function () {};
console.log(f.toString()); // => function () {}

var o = {};
console.log(o.toString()); // => [object Object]

console.log(Object.prototype.isPrototypeOf(o)); // => true
console.log(Function.prototype.isPrototypeOf(o)); // => false

console.log(Object.prototype.isPrototypeOf(f)); // => true
console.log(Function.prototype.isPrototypeOf(f)); // => true

console.log(Object.prototype === Object.getPrototypeOf(f)); // => false
console.log(Function.prototype === Object.getPrototypeOf(f)); // => true

