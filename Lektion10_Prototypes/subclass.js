function Person(name) {this.name = name;}
Person.prototype.toString = function () {
    return this.name;
};
function Student(name, id) {
    Person.call(this, name);
    this.id = id;
}
Student.prototype = Object.create(Person.prototype);
Student.prototype.constructor = Student;
Student.prototype.toString = function () {
    return Person.prototype.toString.call(this) + ": " + this.id;
}
var person = new Person("Charlie Brown");
var student = new Student("Lucy", 112);
console.log(person.toString()); // => Charlie Brown
console.log(student.toString()); // => Lucy: 112

var f = function () { x=6;}

