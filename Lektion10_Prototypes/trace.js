function trace(o, m) {
    var original = o[m];
    o[m] = function () {
        console.log(new Date().getMilliseconds() + ": Entering ", m);
        var result = original.apply(this, arguments);
        console.log(new Date().getMilliseconds() + ": Exiting  ", m);
        return result;
    };
}

