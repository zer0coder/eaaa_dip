var usersException = false;
var usersUrl = 'http://jsonplaceholder.typicode.com/users';

$.ajax({
    dataType: "json",
    url: usersUrl,
    success: function (users) {
        try {
            if (usersException) throw 'users exception';
            console.log('name = ' + users[9].name);
        } catch (e) {
            console.log(e);
        }
    },
    error: function (error) {
        console.log('"' + error.statusText + '" error ');
    }
});
console.log('efter async');

