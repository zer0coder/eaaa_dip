var usersException = false;
var postsException = false;
var usersUrl = 'http://jsonplaceholder.typicode.com/users';
var postsUrl = 'http://jsonplaceholder.typicode.com/posts';

$.getJSON(usersUrl, function (users) {
        try {
            if (usersException) throw 'users exception';
            var userid = users[9].id;
            console.log('user id = ' + userid);
            $.getJSON(postsUrl + '?userId=' + userid, function (posts) {
                    try {
                        if (postsException) throw 'posts exception';
                        console.log('post id = ' + posts[0].id);
                    } catch (e) {
                        console.log(e);
                    }
                }
            );
        } catch (e) {
            console.log(e);
        }
    }
);
console.log('efter async');

