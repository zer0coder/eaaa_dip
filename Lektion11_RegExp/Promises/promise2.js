var usersException = false;
var usersUrl = 'http://jsonplaceholder.typicode.com/users';

$.getJSON(usersUrl)
    .then(function (users) {
            if (usersException) throw 'users exception';
            return users[9].name;
        }
    )
    .then(function (name) {
            console.log('name = ' + name);
        }
    )
    .catch(function (error) {
        log(error);
    });
console.log('efter promise');


function log(error) {
    if (typeof error === 'string') console.log(error);
    else console.log('"' + error.statusText + '" error ');
}
