var usersException = false;
var postsException = false;
var usersUrl = 'http://jsonplaceholder.typicode.com/users';
var postsUrl = 'http://jsonplaceholder.typicode.com/posts';

$.getJSON(usersUrl)
    .then(function (users) {
            if (usersException) throw 'users exception';
            return users[9].id;
        }
    )
    .then(function (userid) {
            console.log('user id = ' + userid);
            return $.getJSON(postsUrl + '?userId=' + userid);
        }
    )
    .then(function (posts) {
        if (postsException) throw 'posts exception';
        console.log('post id = ' + posts[0].id);
        }
    )
    .catch(function (error) {
        log(error);
    });
console.log('efter promise');


function log(error) {
    if (typeof error === 'string') console.log(error);
    else console.log('"' + error.statusText + '" error ');
}

