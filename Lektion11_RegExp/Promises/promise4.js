var usersUrl = 'http://jsonplaceholder.typicode.com/users';
var postsUrl = 'http://jsonplaceholder.typicode.com/posts?userId=10';

Promise.all([$.getJSON(usersUrl), $.getJSON(postsUrl)])
    .then(function (results) {
        console.log('user id = ' + results[0][9].id
               + ' & post id = ' + results[1][0].id);
    })
    .catch(function (error) {
        log(error);
    });
console.log('efter promise');

function log(error) {
    if (typeof error === 'string') console.log(error);
    else console.log('"' + error.statusText + '" error ');
}

