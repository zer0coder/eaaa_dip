var regex = new RegExp("cat|dog");
regex = /cat|dog/;

var str = "The cat's name is Garfield";
console.log(/cat/.test(str)); // => true
console.log(str.search(/cat/)); // => 4
console.log(str.search("cat")); // => 4
console.log(str.search(/[set]/)); // => 2
console.log(str.search(/[e-t]/)); // => 1

var str = "The cat's name is Garfield";
console.log(/cat../.exec(str)[0]); // => cat's
console.log(str.match(/cat../)[0]); // => cat's
console.log(/\W. /.exec(str)[0]); // => 's_ (_ ~ space)
console.log(/\s..\s/.exec(str)[0]); // => _is_

var str = "The cat's name is Garfield";
console.log(/cat.+s/.exec(str)[0]); // => cat's name is
console.log(/cat.+?s/.exec(str)[0]); // => cat's
console.log(/cat?/.exec(str)[0]); // => cat
console.log(/cat??/.exec(str)[0]); // => ca

var str = "The cat's name is Garfield";
console.log(/^.+cat/.exec(str)[0]); // => The cat
console.log(/\b.a.*?\b/.exec(str)[0]); // => cat
console.log(/^[a-zA-Z]+$/.test("No Name")); // => false
console.log(/^[a-zA-Z]{2,} [a-zA-Z]{4,}$/.test("No Name")); // => true

var str = "The cat's name is Garfield";
console.log(/cat|dog/.test(str)); // => true
var matched = /^([a-zA-Z]{2,}) ([a-zA-Z]{4,})$/.exec("No Name");
console.log(matched[0]); // => No Name
console.log(matched[1]); // => No
console.log(matched[2]); // => Name

var str = "The cat's name is Garfield";
console.log(str.replace(/[ ']/, "-"));
// => The-cat's name is Garfield
console.log(str.replace(/[ ']/g, "-"));
// => The-cat-s-name-is-Garfield
console.log("No Name".replace(/^([a-zA-Z]{2,}) ([a-zA-Z]{4,})$/,
    "$2, $1"));
// => Name, No
console.log(str.split(/[ ']/));
// => [ 'The', 'cat', 's', 'name', 'is', 'Garfield' ]

var str = "The cat's name is Garfield";
console.log(str.match(/[a-zA-Z]{4,}/g));
// => [ 'name', 'Garfield' ]
var regex = /[a-z]{4,}/gi;
var matched;
while ((matched = regex.exec(str)) !== null){
    console.log(matched[0]);
}
// => name
// => Garfield

