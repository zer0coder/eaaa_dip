$(function() {
    var postsURL = "http://jsonplaceholder.typicode.com/posts";
    var commentsURL = "http://jsonplaceholder.typicode.com/comments";
    var usersURL = "http://jsonplaceholder.typicode.com/users";

    Promise.all([$.getJSON(postsURL), $.getJSON(commentsURL)])
        .then(function (results) {

            console.log(results[0]);
            console.log(results[1]);

            for (var k = 0; k < results[0].length; k++) {
                for (var i = 0; i < results[1].length; i++) {
                    if (results[0][k].id === results[1][i].postId) {

                        $("#para").append("<p><b>email:</b> "
                            + results[1][i].email
                            + "<br /><b>on post:</b> ID["
                            + results[1][i].postId
                            + "]<br /><b>with topic:</b> "
                            + results[0][k].title + "</p>");
                    }

                }
            }

    }).catch(function (error) {
        console.log(error);
    });

});