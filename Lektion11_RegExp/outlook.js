$(function () {
    var outlook = "Abdikadar Abdi Hagi-Ali (EAAABABD1) <eaaababd1@students.eaaa.dk>; Aleta Miechels Pedersen (EAAAMP) <eaaamp@students.eaaa.dk>; Anders Busk (EAAABU) <eaaabu@students.eaaa.dk>; Asbjørn Christian Bach (EAAACB) <eaaacb@students.eaaa.dk>; Christopher Franklin Jørgensen (EAACHJO) <eaachjo@students.eaaa.dk>; Danial Petursson Olsen (EAADAOLS1) <eaadaols1@students.eaaa.dk>; Francisco Cascales Gracia Gluver (EAAFCGG) <eaafcgg@students.eaaa.dk>; Henrik Østergaard Sunesen (EAAHOS) <eaahos@students.eaaa.dk>; Jacob Findahl Brodersen (EAAJABRO5) <eaajabro5@students.eaaa.dk>; Janus Christiansen (EAAJACHR24) <eaajachr24@students.eaaa.dk>; Jens Skov Knudsen (EAAJEKNU5) <eaajeknu5@students.eaaa.dk>; Jonas Kreutzfeldt Busk Persson (EAAJKBP) <eaajkbp@students.eaaa.dk>; Jonas Møller Winkel (EAAJOWIN3) <eaajowin3@students.eaaa.dk>; Jonathan Birch Jensen (EAAJOJE) <eaajoje@students.eaaa.dk>; Kristian Knudsen Damsgaard (EAAKKD) <eaakkd@students.eaaa.dk>; Kristian Toft Olsen (EAAKTO) <eaakto@students.eaaa.dk>; Lasse Lau Jensen (EAALLJ) <eaallj@students.eaaa.dk>; Mathias Klitt (EAAMAK) <eaamak@students.eaaa.dk>; Mikkel Holmgaard Andersen (EAAMIAND36) <eaamiand36@students.eaaa.dk>; Mohamed Ahmed Farhan (EAAMOFAR1) <eaamofar1@students.eaaa.dk>; Niklas Jan Kranse (EAANIKRA6) <eaanikra6@students.eaaa.dk>; Nikolai Guldager Christensen (EAANGC) <eaangc@students.eaaa.dk>; Nikolaj Tjørnelund Jensen (EAANTJ) <eaantj@students.eaaa.dk>; Paw Forum Christensen (EAAPFC) <eaapfc@students.eaaa.dk>; Sebastian Skovbo Clausen (EAASECLA) <eaasecla@students.eaaa.dk>; Simon Skov Vang (EAASIVAN3) <eaasivan3@students.eaaa.dk>; Søren Ravn Theilgaard (EAASRT) <eaasrt@students.eaaa.dk>; Thomas Lyngsø Sørensen (EAATHSOR20) <eaathsor20@students.eaaa.dk>; Thuan Duc Nguyen (EAATDN) <eaatdn@students.eaaa.dk>; Ulrich Enevoldsen (EAAULENE) <eaaulene@students.eaaa.dk>; Victor Loke Chapelle Hansen (EAAVIHAN2) <eaavihan2@students.eaaa.dk>";

    // var reg = /([^(]+)[(]\w+[)]\s<([^\s]+)>;?\s?/ig;
    // var result = reg.exec(outlook);
    //
    //
    // for(var i = 0; i<30; i++) {
    //     var res = reg.exec(outlook);
    //     $("#maillist").append("<tr>"+ "<td>" + res[0] + "</td>" + "<td>" + res[1] + "</td>" + "<td>" + res[2] + "</td></tr>")
    // }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // var solution = /(^|; )(.+?) \(.+?<(.+?)>/g;
    // var matched;
    // while((matched = solution.exec(outlook)) !== null) {
    //     $("#maillist").append("<tr>"+ "<td>" + matched[2] + "</td>" + "<td>" + matched[1] + "</td>" + "<td>" + matched[3] + "</td></tr>")
    // }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    var splitted = outlook.split(/[;]/ig);

    var matched = [undefined, undefined, undefined];
    var regex = /[\w\s\-]+/i;
    var id = /\(\w+\)/i;
    var email = /<\w+@\w+\.eaaa\.dk>/i;

    splitted.forEach(function (value) {
        matched[0] = regex.exec(value)[0];
        matched[1] = id.exec(value)[0].replace(/[^a-zA-Z0-9 ]/g, "");
        matched[2] = email.exec(value)[0].replace(/[<>]/g, "");
        // console.log(matched);

        $("#maillist").append("<tr>"+ "<td>" + matched[0] + "</td>" + "<td>" + matched[1] + "</td>" + "<td>" + matched[2] + "</td></tr>")
    });


});
