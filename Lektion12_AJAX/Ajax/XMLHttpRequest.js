var url = 'http://jsonplaceholder.typicode.com/users';
var xhr = new XMLHttpRequest();
xhr.responseType = "json";
var async = true;
xhr.open('GET', url, async);
xhr.onreadystatechange = function() {
    if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
        var users = xhr.response;
        for (var i in users)
            console.log(users[i].name);
    }
};
xhr.send();
console.log("efter xhr.send()");

