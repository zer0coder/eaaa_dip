$(function() {

    /////////////////////////////////////////////////////////////////////////////////////
    // USER NAME PLEASE!

    var username = prompt("Brugernavn: ");

    /////////////////////////////////////////////////////////////////////////////////////

    var url = "https://dip-chat-server.herokuapp.com/api/";
    var rooms = {
        messages: "messages"
    };

    var chatArea = $("#chatArea");
    var selector = $("#chatroomSelector");
    var serials = $("#serialSelector");

    // MSG VALUES:  _id, timestamp, serial, roomName, text, name

    /////////////////////////////////////////////////////////////////////////////////////

    $.get(url+rooms.messages, function (data) {

        data.forEach(function (entry) {

            chatArea.val(chatArea.val()+"(" + entry.serial + ") " + entry.name + ": " + entry.text + "\n");

            if(entry.roomName !== undefined) {
                addToChat(entry);
                addSerials(entry);
            }
        });


        refreshChat();
    });

    /////////////////////////////////////////////////////////////////////////////////////

    $("#chatForm").on("submit", function (event) {
        event.preventDefault();
        var txt = $("#inputText");
        var selected = selector.find(":selected").val();
        // console.log(rooms[selected]);

        $.post(url+selected, {
            name: username,
            roomName: selected,
            text: txt.val()
        }).then(function (response) {
            console.log(response);
            txt.val("");
            getChatRoom(selected, undefined);
        }).catch(function (reason) {
            console.log("Indsend fejl: \n" + JSON.stringify(reason));
        });
    });

    selector.change(function() {
        var room = selector.find(":selected").val();

        getChatRoom(room, undefined);
    });

    serials.change(function() {
        var room = selector.find(":selected").val();
        var serial = serials.find(":selected").val();

        getChatRoom(room, serial);
    });

    /////////////////////////////////////////////////////////////////////////////////////

    var getChatRoom = function (room, serial) {
        var path;

        if(room === "messages") {
            path = url+room;
        } else {
            path = url + "messages/" + room;
        }

        if(serial !== "NaN") {
            if(serial !== undefined)
                path = path + "/" + serial;
        }

        console.log(path);

        $.get(path, function (data) {

            var chatText = "";

            serials.find("option").remove().end();

            if(data.length >= 1) {
                data.forEach(function (entry) {
                    chatText += "(" + entry.serial + ") " + entry.name + ": " + entry.text + "\n";

                    if (entry.serial !== undefined) {
                        addSerials(entry);
                    }
                });

                chatArea.val(chatText);
            }
        });
    };

    var addToChat = function (data) {

        selector.append($("<option>", {
            value: data.roomName,
            text: data.roomName
        }));
    };

    var addSerials = function(data) {

        serials.append($("<option>", {
            value: data.serial,
            text: data.serial
        }));

    };

    function refreshChat() {
        var room = selector.find(":selected").val();
        var serial = serials.find(":selected").val();
        getChatRoom(room, serial);
        setTimeout(refreshChat,3000);
    }

    /////////////////////////////////////////////////////////////////////////////////////
});