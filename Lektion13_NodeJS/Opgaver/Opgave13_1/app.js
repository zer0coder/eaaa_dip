var http = require("http");
var fs = require("fs");

http.createServer(function (request, response) {

    response.writeHead(200);
    // console.log(request.url);

    var url_split = request.url.split("/");
    // console.log(url_split);

    if(url_split[1] === "files") {

        var path = "./files/"+(url_split[2].replace(/%20/g, " "));
        // console.log(path);

        fs.readFile(path, function (err, data) {

            if(err)
                console.log(err);

            response.write(data);
            response.end();
        });

    } else {

        fs.readdir("./files/", function (err, files) {
            if(err)
                console.log(err);

            files.forEach(function (value) {
                response.write("<a href=\"./files/" + value + "\">" + value + "</a><br>");
                // response.write("<a href=\"./files/" + value + "\">" + "<img src='files/"+value+"' height='35%' width='35%'>" + "</a><br>");
                // response.write("<img src='files/"+value+"' height='35%' width='35%'><br>", { "Content-Type": "text/html"});
            });

            response.end();
        });
    }

}).listen(8080);