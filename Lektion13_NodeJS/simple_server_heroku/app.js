var http = require('http');

// process.env.PORT lets the port be set by Heroku
var port = process.env.PORT || 8080;

http.createServer(function(request, response) {
    response.writeHead(200);
    response.write("Hello World!");
    response.end();
}).listen(port);

console.log("Listening on port " + port + "...");

