///////////////////////////////////////////////////////////////////////////////////////////
var express = require("express");
var morgan = require("morgan");
var bodyParser = require("body-parser");
var app = express();
///////////////////////////////////////////////////////////////////////////////////////////
app.use(morgan("tiny"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
///////////////////////////////////////////////////////////////////////////////////////////

var printEvents = false;
var messages = [];

addMessage("Hello, World!", "1");
///////////////////////////////////////////////////////////////////////////////////////////
app.get("/", function (req, res) {
    printEvent("Work in Progress");
});
app.get("/messages", function (req, res) {
    printEvent("GET:Messages");

    res.json(messages);
});
app.get("/messages/:id", function (req, res) {
    var id = req.params.id;
    printEvent("GET:Messages:" + id);

    var msg = getMessage(id);

    res.json(msg);
});
app.post("/messages", function (req, res) {
    printEvent("POST:MESSAGES");

    var message = req.body.message;
    var id = req.body.id;

    if(message !== undefined && id !== undefined) {
        addMessage(message, id);

        res.status(201).send("Message created...");
    } else {
        res.status(400).send("Bad Request - Message data undefined.")
    }
});
app.delete("/messages/:id", function (req, res) {
    var id = req.params.id;
    printEvent("DELETE:Message:"+id);

    deleteMessage(id);
    res.status(200).send("Deleted message...");

});
app.put("/messages/:id", function (req, res) {
    var id = req.params.id;
    var message = req.body.message;
    printEvent("PUT:Message:"+id+":"+message);

    if(message !== undefined) {
        updateMessage(id, message);

        res.status(201).send("Message updated...");
    } else {
        res.status(400).send("Bad Request - message undefined.")
    }
});

app.listen(8080);
console.log("Listening on port 8080...");
///////////////////////////////////////////////////////////////////////////////////////////
function addMessage(text, num) {
    messages.push({message: text, id: num});
}
function printEvent(text) {
    if(printEvents)
        console.log(text);
}
function getMessage(id) {
    for(var i = 0; i<messages.length; i++) {
        if(messages[i].id === id) {
            return messages[i];
        }
    }
}
function deleteMessage(id) {
    for(var i = 0; i<messages.length; i++) {
        if(messages[i].id === id) {
            delete messages[i];
            return;
        }
    }
}
function updateMessage(id, text) {
    for(var i = 0; i<messages.length; i++) {
        if(messages[i].id === id) {
            messages[i].message = text;
            return;
        }
    }
}
///////////////////////////////////////////////////////////////////////////////////////////