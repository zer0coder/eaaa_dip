$(function() {

    /////////////////////////////////////////////////////////////////////////////////////
    // USER NAME PLEASE!

    var username = prompt("Brugernavn: ");
    var uid = (username.length%2)+4*3;
    console.log(username+":"+uid);

    /////////////////////////////////////////////////////////////////////////////////////

    var url = "http://localhost:8080/";
    var rooms = {
        messages: "messages"
    };

    var chatArea = $("#chatArea");
    var selector = $("#userSelector");
    // var serials = $("#serialSelector");

    // MSG VALUES:  id, message

    /////////////////////////////////////////////////////////////////////////////////////

    $.get(url+rooms.messages, function (data) {

        data.forEach(function (entry) {

            chatArea.val(chatArea.val()+entry.id + ": " + entry.message + "\n");

        });

        // refreshChat();
    });

    /////////////////////////////////////////////////////////////////////////////////////

    $("#chatForm").on("submit", function (event) {
        event.preventDefault();
        var txt = $("#inputText");
        var selected = selector.find(":selected").val();
        // console.log(rooms[selected]);

        $.post(url+selected, {
            id: uid,
            message: txt.val()
        }).then(function (response) {
            console.log(response);
            txt.val("");
            getChatRoom(selected);
        }).catch(function (reason) {
            console.log("Indsend fejl: \n" + JSON.stringify(reason));
        });
    });

    selector.change(function() {
        var room = selector.find(":selected").val();

        getChatRoom(room);
    });

    /////////////////////////////////////////////////////////////////////////////////////

    var getChatRoom = function (room) {
        var path = url + room;

        console.log(path);

        $.get(path, function (data) {

            var chatText = "";

            if(data.length >= 1) {
                data.forEach(function (entry) {
                    chatText += entry.id + ": " + entry.message + "\n";
                });

                chatArea.val(chatText);
            }
        });
    };

    // var addToChat = function (data) {
    //
    //     selector.append($("<option>", {
    //         value: data.roomName,
    //         text: data.roomName
    //     }));
    // };

    function refreshChat() {
        var room = selector.find(":selected").val();

        getChatRoom(room);
        setTimeout(refreshChat,3000);
    }

    /////////////////////////////////////////////////////////////////////////////////////
});