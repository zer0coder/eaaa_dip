var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/', function(req, res) {
    res.send("You said: " + req.body.text1 +
        ", " + req.body.text2);
}).listen(8080);

console.log("Listening on port 8080 ...");

