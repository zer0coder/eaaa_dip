$(function () {
    $('#btn').click(function (event) {
        var data = {
            text1: $('#text1').val(),
            text2: $('#text2').val()
        };
        $.post('/', data)
        .then(function (data) {
            $('body').append("<br>").append(data);
        })
        .catch(function (err) {
            console.log("Error: " + err);
        });
    });
});

