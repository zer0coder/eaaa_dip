var express = require('express');
var app = express();
var morgan = require('morgan');

app.use(morgan('tiny'));

app.get('/test/:text', function(request, response) {
    var parameter = request.params.text;
    response.send("Parameter: " + parameter);

}).listen(8080);

console.log("Listening on port 8080 ...");

