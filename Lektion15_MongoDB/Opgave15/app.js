///////////////////////////////////////////////////////////////////////////////////////////
var express = require("express");
var morgan = require("morgan");
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var app = express();
///////////////////////////////////////////////////////////////////////////////////////////
app.use(morgan("tiny"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static("public"));

var dbuser = "opgave15";
var dbpassword = "lektion15";

var databaseURL = "mongodb://localhost/messages";
// var databaseURL = "mongodb://" + dbuser + ":" + dbpassword + "@ds033255.mlab.com:33255/opgave15";

mongoose.Promise = global.Promise;
mongoose.connect(databaseURL, {
    useMongoClient: true
});
///////////////////////////////////////////////////////////////////////////////////////////

var printEvents = false;
var Message = require("./models/message");

///////////////////////////////////////////////////////////////////////////////////////////
app.get("/messages", function (req, res) {
    printEvent("GET:Messages");

    Message.find(function (err, messages) {
        if (err) {
            res.send(err);
        } else {
            res.json(messages);
        }
    })
})
.get("/messages/:name", function (req, res) {
    var name = req.params.name;

    printEvent("GET:Messages:" + name);

    Message.find({ name: name }, function (err, messages) {
        if(err) {
            res.send(err);
        } else {
            res.json(messages);
        }
    });
})
.post("/messages", function (req, res) {
    printEvent("POST:MESSAGES");

    var message = new Message({
        message: req.body.message,
        name: req.body.name.toLowerCase(),
        date: req.body.date
    });

    if(message !== undefined) {

        message.save(function (err) {
            if(err) {
                res.status(500);
                res.send(err);
            } else {
                res.status(201).send("Message created...");
            }
        })

    } else {
        res.status(400).send("Bad Request - Message data undefined.")
    }
})
.delete("/messages/:name/:id", function (req, res) {
    var id = req.params.id;
    printEvent("DELETE:Message:"+id);

    Message.remove({ _id: id }, function (err) {
        if(err) {
            res.send(err);
        } else {
            res.status(200).send("Deleted message...");
        }
    });
})
.put("/messages/:name/:id", function (req, res) {
    var id = req.params.id;
    var message = req.body.message;
    printEvent("PUT:Message:"+id+":"+message);

    if(message !== undefined) {

        Message.update({ _id: id }, { "$set": { message: message } }, function(err) {
            if(err) {
                res.send(err);
            } else {
                res.status(201).send("Message updated...");
            }
        })

    } else {
        res.status(400).send("Bad Request - message undefined.")
    }
});

app.param("name", function(req, res, next) {
    req.params.name = req.params.name.toLowerCase();
    next();
});

app.listen(8080);
console.log("Listening on port 8080...");
///////////////////////////////////////////////////////////////////////////////////////////

function printEvent(text) {
    if(printEvents)
        console.log(text);
}

///////////////////////////////////////////////////////////////////////////////////////////