var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var message = new Schema({
    message: String,
    name: String,
    date: Date
});

message.methods.printMessage = function () {
    console.log(this.name + ": " + this.message);
};

module.exports = mongoose.model("Message", message);