$(function() {

    /////////////////////////////////////////////////////////////////////////////////////
    // USER NAME PLEASE!

    var username = prompt("Brugernavn: ");
    console.log(username);

    /////////////////////////////////////////////////////////////////////////////////////

    var url = "http://localhost:8080/";
    var rooms = {
        messages: "messages"
    };

    var messageIDs = [];

    var chatArea = $("#chatArea");
    var userSelector = $("#userSelector");
    var messageSelector = $("#messageSelector");

    // MSG VALUES:  id, message

    /////////////////////////////////////////////////////////////////////////////////////

    $.get(url+rooms.messages, function (data) {

        data.forEach(function (entry) {

            chatArea.val(chatArea.val()+"("+entry.date+") "+entry.name + ": " + entry.message + "\n");
            addMessageIDtoArray(entry._id);

            if(entry.name !== undefined) {
                addUserToOptions(entry);
                // addSerials(entry);
            }
        });

        // refreshChat();
    });

    /////////////////////////////////////////////////////////////////////////////////////

    $("#chatForm").on("submit", function (event) {
        event.preventDefault();
        var txt = $("#inputText");
        var selected = userSelector.find(":selected").val();

        $.post(url+rooms.messages, {
            message: txt.val(),
            name: username,
            date: Date.now()
        }).then(function (response) {
            console.log(response);
            txt.val("");
            getChatRoom(selected);
        }).catch(function (reason) {
            console.log("Indsend fejl: \n" + JSON.stringify(reason));
        });
    });

    $("#updateText").on("click", function (event) {
        console.log("UPDATE");
        event.preventDefault();
        var txt = $("#inputText");
        var userSelected = userSelector.find(":selected").val();
        var messageSelected = messageSelector.find(":selected").val();

        $.ajax({
            url: url+rooms.messages+"/"+userSelected+"/"+messageSelected,
            method: "PUT",
            data: { message: txt.val(), edited: Date.now() },
            dataType: "json"

        }).then(function (response) {
            console.log(response);
            txt.val("");
            getChatRoom(userSelected);
        }).catch(function (reason) {
            console.log("Update fejl: \n" + JSON.stringify(reason));
        });

    });

    $("#deleteMessage").on("click", function (event) {
        console.log("DELETE");
        event.preventDefault();
        var userSelected = userSelector.find(":selected").val();
        var messageSelected = messageSelector.find(":selected").val();

        $.ajax({
            url: url+rooms.messages+"/"+userSelected+"/"+messageSelected,
            method: "DELETE"
        }).then(function (response) {
            console.log(response);
            getChatRoom(userSelected);
        }).catch(function (reason) {
            console.log("Delete fejl: \n" + JSON.stringify(reason));
        });

    });

    userSelector.change(function() {
        var room = userSelector.find(":selected").val();

        getChatRoom(room);
    });

    /////////////////////////////////////////////////////////////////////////////////////

    var getChatRoom = function (room) {
        var path;

        if(room === "messages") {
            path = url+room;
        } else {
            path = url + "messages/" + room;
        }

        console.log(path);

        $.get(path, function (data) {

            var chatText = "";

            messageSelector.find("option").remove().end();

            if(data.length >= 1) {
                data.forEach(function (entry) {
                    chatText += "("+entry.date+") "+entry.name + ": " + entry.message + "\n";

                    if (entry._id !== undefined && room !== "messages") {
                        addMessageIDTtoOptions(entry);
                    }
                });

                chatArea.val(chatText);
            }
        });
    };

    var addMessageIDTtoOptions = function(data) {

        messageSelector.append($("<option>", {
            value: data._id,
            text: data.message
        }));

    };

    var addUserToOptions = function (data) {

        userSelector.append($("<option>", {
            value: data.name,
            text: data.name
        }));
    };

    function refreshChat() {
        var room = userSelector.find(":selected").val();

        getChatRoom(room);
        setTimeout(refreshChat,3000);
    }

    function addMessageIDtoArray(uid) {
        messageIDs.push(uid);
    }

    /////////////////////////////////////////////////////////////////////////////////////
});