var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(morgan('short'));

var dbuser = "cars";
var dbpass = "lektion15";

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://' + dbuser + ':' + dbpass + '@ds131989.mlab.com:31989/fccg_cars', {
    useMongoClient: true
});

var Car = require('./models/car');

app.get('/cars', function (req, res) {
    Car.find(function (err, cars) {
        if (err) {
            res.send(err);
        } else {
            res.json(cars);
        }
    });
});

app.post('/cars', function (req, res) {
    var car = new Car({
        year: req.body.year,
        brand: req.body.brand,
        model: req.body.model
    });
    car.printCar();
    car.save(function (err) {
        if (err) {
            res.status(500);
            res.send(err);
        } else {
            res.json({message: 'Car saved!'});
        }
    });
});

app.listen(8080);

console.log('Listening on port 8080 ...');

