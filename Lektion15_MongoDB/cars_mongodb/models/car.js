var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var car = new Schema({
    year: Number,
    brand: String,
    model: String
});
car.methods.printCar = function() {
    console.log(this.brand + " " + this.model);
};

module.exports = mongoose.model('Car', car);

