"use strict";

// INITIALIZATION
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var config = require('./config');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(morgan('tiny'));
app.use(express.static('public'));

// MONGODB & MONGOOSE SETUP
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var mongoConnection = mongoose.connect(config.mongoDBhost).connection;
mongoConnection.on('error', function(err) {
    console.error(err.message);
});

// ROUTES FOR THE APP
var companyRouter = require('./routes/company')(express);
var employeeRouter = require('./routes/employee')(express);
app.use(companyRouter);
app.use(employeeRouter);

// START THE SERVER
var port = process.env.PORT || config.defaultPort;
app.listen(port)
console.log('Listening on port ' + port + ' ...');

