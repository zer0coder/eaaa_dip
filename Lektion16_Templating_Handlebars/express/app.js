var fs = require('fs');
var express = require('express');
var app = express();

app.use(express.static('public'));

app.get('/', function(request, response) {
        fs.readdir("public", function (err, files) {
            if (err) throw err;
            var html = "";
            for (var k in files) {
                html += '<a href="' + files[k] + '">' + files[k] + '</a><br>\n';
            }
            response.send(html);
        })
}).listen(8080);

console.log("Listening on port 8080 ...");

