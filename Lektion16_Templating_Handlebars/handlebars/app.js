var fs = require('fs');
var express = require('express');
var app = express();

app.use(express.static('public'));

app.get('/files', function (request, response) {
    fs.readdir("public", function (err, files) {
        if (err) throw err;
        response.json({"files": files});
    })
}).listen(8080);

console.log("Listening on port 8080 ...");

