$(function () {
    $.get('files.hbs')
        .then(function (template) {
            var compiled = Handlebars.compile(template);
            $.getJSON('/files')
                .then(function (data) {
                    var html = compiled(data);
                    $('body').append(html);
                });
        })
});

