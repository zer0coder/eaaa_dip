var fs = require('fs');
var express = require('express');
var app = express();
var hbs = require('hbs');
hbs.registerPartials('templates');

app.set('view engine', 'hbs');
app.set('views', 'templates');

app.use(express.static('public'));

app.get('/', function (request, response) {
    fs.readdir("public", function (err, files) {
        if (err) throw err;
        response.render('index',
            {
                "title": "Files",
                "header": "Files in public directory:",
                "files": files
            });
    })
}).listen(8080);

console.log("Listening on port 8080 ...");

