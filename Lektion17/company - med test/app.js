"use strict";

// INITIALIZATION
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var config = require('./config');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(morgan('tiny'));
app.use(express.static('public'));

// MONGODB & MONGOOSE SETUP
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(config.mongoDBhost + '/companyDB');

// ROUTES FOR THE APP
var companyRouter = require('./routes/company')(express);
var employeeRouter = require('./routes/employee')(express);
app.use(companyRouter);
app.use(employeeRouter);

// START THE SERVER
var port = process.env.PORT || config.defaultPort;
app.listen(port);
console.log('Listening on port ' + port + ' ...');

module.exports = app;

