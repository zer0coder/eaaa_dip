var express		=	require('express');
var session		=	require('express-session');
var bodyParser  = 	require('body-parser');
var app			=	express();
var morgan = require('morgan');
/////////////////////////////////////////////////////////////////////
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(morgan('tiny'));
app.use(express.static("public"));
app.use(session({secret: 'ssshhhhh',saveUninitialized: true, resave: true}));
/////////////////////////////////////////////////////////////////////

var sess;

/////////////////////////////////////////////////////////////////////
app.get("/", function (req, res) {

    sess = req.session;
    res.send("Session:\t" + JSON.stringify(sess));

});

app.get("/admin", function (req, res) {

    sess = req.session;

    if (sess.views) {
        sess.views++;
        res.setHeader('Content-Type', 'text/html');
        res.write('<p>views: ' + sess.views + '</p>');
        res.end();
    } else {
        sess.views = 1;
        res.end('welcome to the session demo. refresh!');
    }

});

app.get("/reset", function (req, res) {

    req.session.destroy(function (err) {
        if(err){
            console.log(err);
        }
        else
        {
            res.redirect('/');
        }
    });

});

/////////////////////////////////////////////////////////////////////
app.listen(8080);
console.log("Listening on port 8080 ...");
/////////////////////////////////////////////////////////////////////