var list = [7, 13, 9, 8, 4, 1, 2, 16, 0];

var i, j;

for (i = list.length - 1; i >= 0; i--) {
    for (j = 0; j <= i - 1; j++) {
        if (list[j] > list[j + 1]) {
            var temp = list[j];
            list[j] = list[j+1];
            list[j+1] = temp;
        }
    }
}

console.log(list); // => 0,1,2,4,7,8,9,13,16
