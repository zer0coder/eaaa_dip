var list = [7, 13, 9, 8, 4, 1, 2, 16, 0];

var i, j;

for (i = list.length - 1; i >= 0; i--) {
    for (j = 0; j <= i - 1; j++) {
        if (list[j] > list[j + 1]) {
            var temp = list[j];
            list[j] = list[j+1];
            list[j+1] = temp;
        }
    }
}

console.log(list); // => 0,1,2,4,7,8,9,13,16

function binary(input, array) {
    var mid = Math.floor(array.length / 2);
    // console.log("Mid: " + list[mid]);
    var k = array[mid];

    if(k === input) {
        console.log("Found!", k, input);
        return k;
    } else if (input < k && array.length > 1) {
        return binary(input, array.slice(0, mid));
    } else if (input > k && array.length > 1) {
        return binary(input, array.slice(mid, Number.MAX_VALUE));
    } else {
        console.log("Not found!", input);
        return -1;
    }
}
// function binary(input, list) {
//     var left  = 0,
//         right   = list.length - 1,
//         mid = Math.floor((right + left)/2);
//
//     while(list[mid] !== input && left < right)
//     {
//         if (input < list[mid])
//         {
//             right = mid - 1;
//         }
//         else if (input > list[mid])
//         {
//             left = mid + 1;
//         }
//         mid = Math.floor((right + left)/2);
//     }
//
//     return (list[mid] !== input) ? -1 : list[mid];
// }

// list => 0,1,2,4,7,8,9,13,16
console.log(binary(0, list));
console.log(binary(1, list));
console.log(binary(2, list));
console.log(binary(4, list));
console.log(binary(7, list));
console.log(binary(8, list));
console.log(binary(9, list));
console.log(binary(13, list));
console.log(binary(16, list));
console.log(binary(18, list));