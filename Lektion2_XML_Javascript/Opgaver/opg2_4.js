var list = ["B", "C", "D", "A", "H", "F", "E", "L", "K"];

var i, j;

for (i = list.length - 1; i >= 0; i--) {
    for (j = 0; j <= i - 1; j++) {
        if (list[j] > list[j + 1]) {
            var temp = list[j];
            list[j] = list[j+1];
            list[j+1] = temp;
        }
    }
}

console.log(list); // A, B, C, D, E, F, H, K, L