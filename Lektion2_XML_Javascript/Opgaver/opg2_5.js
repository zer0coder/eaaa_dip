var list =  ["B", "C", "D", "A", "G", "F", "E", "J", "I"];
var list2 = ["P", "O", "M", "S", "N", "T", "W", "U", "X"];


function bubblesort(input) {
    var i, j;

    for (i = input.length - 1; i >= 0; i--) {
        for (j = 0; j <= i - 1; j++) {
            if (input[j] > input[j + 1]) {
                var temp = input[j];
                input[j] = input[j + 1];
                input[j + 1] = temp;
            }
        }
    }

    return input;
}

console.log(bubblesort(list));
console.log(bubblesort(list2));

function merge(list, low, middle, high) {
    var temp = [];
    var left = low;			// Left index
    var right = middle+1;	// Right index

    // We will go through both sides of the list and check each index-element
    // against each other. The recursion will ensure that the list will be
    // sorted correctly, as we go through the list an X amount of times.
    while(left<=middle && right<=high){
        if(list[left] <= list[right]){
            temp.push(list[left]);
            left++;
        }else{
            temp.push(list[right]);
            right++;
        }
    }

    // We add the remaining index-elements into the temp list.
    while(left <= middle){
        temp.push(list[left]);
        left++;
    }

    while(right<=high){
        temp.push(list[right]);
        right++;
    }

    // We correct the list so that it equals our TEMP list. This helps ensuring that, if/when
    // we go through the list again, we will not mess up the sorted order.
    var i = 0;
    var j = low;

    while(i<temp.length){
        list[j] = temp[i++];
        j++;
    }
}

function mergeSort(list, l, h) {
    if (l < h) {
        var m = Math.floor((l + h) / 2);
        mergeSort(list, l, m);
        mergeSort(list, m + 1, h);
        merge(list, l, m, h);
    }
}
var merged = list.concat(list2);
mergeSort(merged, 0, merged.length - 1);

console.log(merged);
