var a = [0, "one", true, null, undefined];
console.log(a); // => [ 0, 'one', true, null, undefined ]
console.log(a[1]); // => one
console.log(a[7]); // => undefined

var b = [0,1,2,3];
console.log(b.length); // => 4
b.push(4);
console.log(b.shift()); // => 0
console.log(b); // => [ 1, 2, 3, 4 ]

var c = [0, 1];
c[4] = 4;
console.log(c); // => [ 0, 1, , , 4 ]

console.log(typeof []); // => object
console.log(Array.isArray([])); // => true

var d = [0, 1];
console.log(d.length); // => 2
d.length = 1;
console.log(d); // => [ 0 ]
d[2] = 2;
console.log(d); // => [ 0, , 2 ]
d.length = 4;
console.log(d); // => [ 0, , 2,  ]

var e = [0, 1, 2, 3];
e[4] = 4;
var x = e[1];
e[2] = "two";
delete e[3];
console.log(e); // => [ 0, 1, 'two', , 4 ]

var f = [0, 1, 2, 3, 4];
delete f[2];

var text = "";
for (var i = 0; i < f.length; i++)
    text += f[i] + ", ";
console.log(text); // => 0, 1, undefined, 3, 4,

text = "";
for (i in f)
    text += f[i] + ", ";
console.log(text); // => 0, 1, 3, 4,

