function f(i) {
    return 2 * i;
}
console.log(f(3));
// => 6

var g = function(i){
    return ++i;
};
console.log(g(3));
// => 4

console.log(h(3)); // => 3 (h is hoisted)
function h(i) {return i;}
var v = function(i) {return -i;};
console.log(v(3)); // => -3
h = v;
console.log(h(3)); // => -3

var x = "global";
p();
function p() {
    var x = "local";
    console.log(x); // => local
    console.log(this.x); // => global or undefined
}

function sum() {
    var i, sum = 0;
    for (i = 0; i < arguments.length; i += 1) {
        sum += arguments[i];
    }
    return sum;
}
console.log(sum(7, 9, 13)); // => 29

function q(a,b) {
    console.log(a);
    console.log(b);
    console.log(arguments.length);
    console.log(q.length);
}
q("a");
// => a
// => undefined
// => 1
// => 2

function r(x) {
    x = x || 0;
    return x;
}
console.log(r()); // => 0

function s(x) {
    console.log(x || 0);
}
console.log(s(5));
// => 5
// => undefined

var y = "global";
function outer() {
    var y = "outer";
    var outerArguments = arguments;
    var that = this;
    console.log(y); // => outer
    inner();
    function inner() {
        var y = "inner";
        console.log(y); // => inner
        console.log(this.y); // => global or undefined
        console.log(that.y); // => global or undefined
        console.log(outerArguments.length); // => 1
    }
}
outer("test");

function t(a) {
    if (typeof a !== "number" || arguments.length !== t.length)
        throw "Error";
    return a;
}
console.log(t(7)); // => 7
console.log(t(true)); // => Error
console.log(t(9,13)); // => Error



