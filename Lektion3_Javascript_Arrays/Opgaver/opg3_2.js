function fib(n) {
    if(n === 0 || n === 1) return n;
    else return fib(n-1) + fib(n-2);
}

console.log(fib(7)); // fib(7) = 13;

var array = [9, 22, 44, 5, 2, 13, 99, 56, 33, 69, 85]; // max = 99;

function max(list) {
    var max = 0;
    for(var i = 0; i<list.length; i++) {
        if(max < list[i]) {
            max = list[i];
        }
    }
    return max;
}

console.log(max(array)); // 99

function contains(list, element) {
    for(var i = 0; i<list.length; i++) {
        if(list[i] === element) {
            return true;
        }
    }
    return false;
}

console.log(contains(array, 56)); // true
console.log(contains(array, 77)); // false

function sum(list) {
    var total = 0;
    for(var i = 0; i<list.length; i++) {
        if(typeof list[i] === 'number') {
            total += list[i];
        }
    }
    return total;
}

console.log(sum(array));