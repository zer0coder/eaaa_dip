//////////////////////////////////////////////////////////////////////////////////////////////
var list = ["B", "C", "D", "A", "H", "F", "E", "I", "G"];
var list2 = [2, "C", 4, "A", "H", "F", "E", "I", 7];
//////////////////////////////////////////////////////////////////////////////////////////////
var bubbleSort = function (array) {
    if(Array.isArray(array)) {
        var list = array;
        var i, j;

        function swap(i, j) {
            var temp = list[j];
            list[j] = list[j + 1];
            list[j + 1] = temp;
        }

        for (i = list.length - 1; i >= 0; i--) {
            for (j = 0; j <= i - 1; j++) {
                if (typeof list[j] !== typeof list[j + 1])
                    throw "Multiple element types in array!";

                if (list[j] > list[j + 1]) {
                    swap(i, j);
                }
            }
        }
        return list;
    } else {
        throw "List is not an array!";
    }
};
//////////////////////////////////////////////////////////////////////////////////////////////
function binary(input, array) {
    if(Array.isArray(array)) {
        var mid = Math.floor(array.length / 2);
        // console.log("Mid: " + list[mid]);
        var k = array[mid];

        if(typeof k !== input)
            throw "Multiple element types in array!";

        if(k === input) {
            console.log("Found!", k, input);
            return k;
        } else if (input < k && array.length > 1) {
            return binary(input, array.slice(0, mid));
        } else if (input > k && array.length > 1) {
            return binary(input, array.slice(mid, Number.MAX_VALUE));
        } else {
            console.log("Not found!", input);
            return -1;
        }
    } else {
        throw "List is not an array!";
    }
}
//////////////////////////////////////////////////////////////////////////////////////////////
console.log(bubbleSort(list)); // A, B, C, D, E, F, G, H, I
// console.log(bubbleSort(list2)); // throws exception

console.log(binary("I", list));
console.log(binary('A', list));

// console.log(binary(7, list2)); // throws exception
// console.log(binary('C', list2)); // throws exception

