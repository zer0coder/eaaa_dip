////////////////////////////////////////////////////////////////////////////////////////////////
var bubbleSort = function (array, compa) {
    var list = array;
    var i, j;

    function swap(i, j) {
        var temp = list[j];
        list[j] = list[j+1];
        list[j+1] = temp;
    }

    for (i = list.length - 1; i >= 0; i--) {
        for (j = 0; j <= i - 1; j++) {
            if(compa(list[j],list[j+1]) > 0) {
                swap(i,j);
            }
            // if (list[j] > list[j + 1]) {
            //     swap(i,j);
            // }
        }
    }
    return list;
};
////////////////////////////////////////////////////////////////////////////////////////////////
var comp1 = "Hello";
var comp2 = "Hello";
var comp3 = "World";
var comp4 = "Testing";
var array = ["IJ", "GH", "EF", "CD", "AB"];
var array2 = ["ij", "GH", "ef", "CD", "ab"];
var array3 = ["ij", "GH", "ef", "CD", "ab"];
////////////////////////////////////////////////////////////////////////////////////////////////
function compare(s1, s2) {
    if (typeof s1 === "string" && typeof s2 === "string") {

        // return s1.localeCompare(s2); // easy solution

        for(var i = 0; i<s1.length; i++) {
            for(var j = 0; j<s2.length; j++) {
                if(s1[i] < s2[j])
                    return -1;
                else if (s1[i] > s2[j])
                    return 1;
            }
        }
        return 0;

    } else {
        throw "Not the same types!";
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////
function compareLen(s1, s2) {
    if (typeof s1 === typeof s2) {

        // return s1.localeCompare(s2); // easy solution
        // return s1.length - s2.length;

        if(s1.length > s2.length)
            return 1;
        else if (s1.length < s2.length)
            return -1;
        else
            return 0;

    } else {
        throw "Not the same types!";
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////
function compareIgnoreCase(s1, s2) {
    if (typeof s1 === "string" && typeof s2 === "string") {

        var t1 = s1.toUpperCase();
        var t2 = s2.toUpperCase();

        for(var i = 0; i<t1.length; i++) {
            for(var j = 0; j<t2.length; j++) {
                if(t1[i] < t2[j])
                    return -1;
                else if (t1[i] > t2[j])
                    return 1;
            }
        }
        return 0;

    } else {
        throw "Not the same types!";
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////
console.log("COMPARE");
console.log("Function:     " + compare(comp1, comp2));
console.log("LocalCompare: " + comp1.localeCompare(comp2));

console.log("Function:     " + compare(comp1, comp3));
console.log("LocalCompare: " + comp1.localeCompare(comp3));
console.log();
////////////////////////////////////////////////////////////////////////////////////////////////
console.log("COMPARE_LEN");
console.log("Function:     " + compareLen(comp1, comp2));
console.log("LocalCompare: " + comp1.localeCompare(comp2));

console.log("Function:     " + compareLen(comp1, comp4));
console.log("LocalCompare: " + comp1.localeCompare(comp4));
console.log();
////////////////////////////////////////////////////////////////////////////////////////////////
console.log("COMPARE_IGNORE_CASE");
console.log("Function:     " + compareIgnoreCase(comp1, comp2));
console.log("LocalCompare: " + comp1.localeCompare(comp2));

console.log("Function:     " + compareIgnoreCase(comp1, comp3));
console.log("LocalCompare: " + comp1.localeCompare(comp3));
console.log();
////////////////////////////////////////////////////////////////////////////////////////////////
console.log("[bubbleSort with Compare]");
console.log("Unsorted: " + array);
console.log("Sorted:   " + bubbleSort(array, compare));
console.log();

console.log("[bubbleSort with Compare]");
console.log("Unsorted: " + array2);
console.log("Sorted:   " + bubbleSort(array2, compare));
console.log();

console.log("[bubbleSort with CompareIgnoreCase]");
console.log("Unsorted: " + array3);
console.log("Sorted:   " + bubbleSort(array3, compareIgnoreCase));
console.log();
////////////////////////////////////////////////////////////////////////////////////////////////