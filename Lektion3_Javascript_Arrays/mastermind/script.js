"use strict";

var activeRow = 0;
var secret = createSecret();
var pinsSet = 0;
var won = false;

function createSecret() {
    var colors = ["red", "blue", "yellow", "green", "purple", "orange"];
    var res = [];
    for (var i = 0; i < 4; i++) {
        res.push(colors[Math.round(Math.random() * 5)]);
    }
    // return res;
    return ["red","red","yellow","yellow"];
}

function handleRow(row, column) {
    if(row === activeRow && activeRow <= mm.rows) {
        var selcol = mm.getSelectedColor();
        mm.addPin(selcol, row, column);
        pinsSet++;

        if(pinsSet === 4) {
            checkFullRow();
            pinsSet = 0;
            activeRow++;
            if(activeRow > mm.rows && !won) {
                mm.setMessage("You Lost! Game ended.")
            } else if (won) {
                mm.setMessage("You Won, congratulations!")
            }
            console.log(activeRow);
        }
    }
}

function checkFullRow() {
    var curRow = mm.getRow(activeRow);
    var currentColors = [];
    var correctColor = ["", "", "", ""];
    var correctPins = ["", "", "", ""];

    for(var l = 0; l<curRow.length; l++) {
        if(!contains(currentColors, curRow[l])) {
            currentColors.push(curRow[l]);
        }
    }

    for(var i = 0; i<curRow.length; i++) {
        if(curRow[i] === secret[i]) {
            correctPins[i] = "X";
        } else {
            correctPins[i] = "O";
        }

        if(contains(currentColors, secret[i])) {
            correctColor[i] = "X";
        } else {
            correctColor[i] = "O";
        }
    }


    console.log("SECRET: " + secret);
    console.log("COLOR:  " + correctColor);

    console.log("PINS:   " + correctPins);

    mm.addStatus(correctPins, correctColor, activeRow);
    var correct = 0;

    for(var j = 0; j<correctPins.length; j++) {
        if(correctPins[j] === "X") {
            correct++;
        }
    }

    if(correct === 4) {
        won = true;
        activeRow = 10000;
    }
}

function contains(list, element) {
    for(var i = 0; i<list.length; i++) {
        if(list[i] === element) {
            return true;
        }
    }
    return false;
}