"use strict";

/**
 * Validates parenthesis balancing in the inputText element and writes a
 * message to the web page with the result.
 */
function validate() {
    var value = document.getElementById('inputText').value;
    var stack = [];
    var ch;
    for(var i = 0; i<value.length; i++) {
        ch = value.charAt(i);
        if(ch==='(' || ch==='{' || ch==='[') {
            stack.push(ch);
        } else if (ch===')' && stack[stack.length-1] === '(') {
            stack.pop();
        } else if (ch==='}' && stack[stack.length-1] === '{') {
            stack.pop();
        } else if (ch===']' && stack[stack.length-1] === '[') {
            stack.pop();
        }
    }

    if(stack.length === 0)
        message("Everything is balanced!");
    else
        message("Not balanced!");
}

function message(text) {
    document.getElementById('output').innerHTML = text;
}