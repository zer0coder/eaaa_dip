var a = [30, 4, 10, 2, 0];
a.sort();
console.log(a); // => [ 0, 10, 2, 30, 4 ]
a.sort(function(a,b){return a-b;});
console.log(a); // => [ 0, 2, 4, 10, 30 ]
a.sort(function(a,b){return b-a;});
console.log(a); // => [ 30, 10, 4, 2, 0 ]

var a = [1,2,3,4];
console.log(2 in a); // => true
delete a[2];
console.log(2 in a); // => false
console.log(a); // => [ 1, 2, , 4 ]

var o = {a: 1, "2": "two"};
console.log("a" in o);  // => true
console.log(2 in o);  // => true
console.log("b" in o);  // => false
console.log(o); // => { '2': 'two', a: 1 }

var a = [0, 1, 2];
a[-1] = "-1";
a.size = function(){return this.length;};
console.log(a); // => [ 0, 1, 2, '-1': '-1', size: [Function] ]
console.log(a.size()); // => 3
for (var i in a)
    console.log(a[i]);
// => 0
// => 1
// => 2
// => -1
// => [Function]

var s = "abcd";
var t = "";
for (var i in s)
    t += s[i] + ",";
console.log(t); // => a,b,c,d,

