var object = {name: "Tarzan", "high scores": [7, 9, 13]};

var o = {a: 1, "b": 2, "3": "three"};

console.log(o.a); // => 1
console.log(o["a"]); // => 1
// console.log(o[a]); // ReferenceError: a is not defined

console.log(o.b); // => 2
console.log(o["b"]); // => 2

// console.log(o.3); // Compile error
console.log(o["3"]); // => three
console.log(o[3]); // => three

var s = "b";
console.log(o.a, o.b, o.s, o[s]); // => 1 2 undefined 2

var o1 = {x: 1};
var o2 = {x: 1};
console.log(o1 == o2); // => false
var o3 = o1;
console.log(o1 == o3); // => true
o3.x = 3;
console.log(o1.x); // => 3

var o = {a: 1, "b": 2, "3": "three"};
o.c = 4;
var x = o.b;
o["3"] = "III";
delete o.b;
console.log(o); // => { '3': 'III', a: 1, c: 4 }
console.log("a" in o);  // => true
console.log("b" in o);  // => false
for (var p in o)
    console.log(p + ": " + o[p]);
// => 3: III
// => a: 1
// => c: 4

var x = 123;
var o = {x: 456, m: function(){return this.x;}};
console.log(o.m()); // => 456
console.log(o["m"]()); // => 456
var f = o.m;
console.log(f()); // => 123 or undefined
var g = function(){return x};
console.log(g()); // => 123
o.m = g;
console.log(o.m()); // => 123

// var x = 123;
// var y = 456;

var my = {};
my.x = 123;
my.y = 456;

var s = "test";
s.x = 123;
console.log(s.x); // => undefined
console.log(s.length); // => 4
console.log(s[2]); // => s
console.log(s.toUpperCase()); // => TEST

console.log(typeof 123); // => number
console.log(typeof "abc"); // => string
console.log(typeof true); // => boolean
console.log(typeof null); // => object
console.log(typeof undefined); // => undefined
console.log(typeof {}); // => object
console.log(typeof function(){}); // => function
console.log(typeof []); // => object

