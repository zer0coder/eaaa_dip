var personer = [];
// insert
personer[0] = {navn: "Francisco", email: "fgluver@gmail.com", tlf: 80808080};
personer[1] = {navn: "Gluver", email: "hots@gmail.com", tlf: 99999999};
// print
console.log(personer);
// read
var x = personer[0];
// print
console.log(x);
console.log(x.navn); // get property: navn;
// delete
delete personer[0]; // empty item????????
// print
console.log(personer);
console.log(x);
// update
personer[1] = 1337; // update person[1] til number: 1337
// print
console.log(personer);
console.log(x); // x er stadig person object.