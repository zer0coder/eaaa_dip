function compare(s1, s2) {
    if (typeof s1 === "string" && typeof s2 === "string") {

        for(var i = 0; i<s1.length; i++) {
            for(var j = 0; j<s2.length; j++) {
                if(s1[i] < s2[j])
                    return -1;
                else if (s1[i] > s2[j])
                    return 1;
            }
        }
        return 0;

    } else {
        throw "Not the same types!";
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////
function compareLen(s1, s2) {
    if (typeof s1 === "string" && typeof s2 === "string") {

        return s1.length - s2.length;

    } else {
        throw "Not the same types!";
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////
function compareIgnoreCase(s1, s2) {
    if (typeof s1 === "string" && typeof s2 === "string") {

        var t1 = s1.toUpperCase();
        var t2 = s2.toUpperCase();

        for(var i = 0; i<t1.length; i++) {
            for(var j = 0; j<t2.length; j++) {
                if(t1[i] < t2[j])
                    return -1;
                else if (t1[i] > t2[j])
                    return 1;
            }
        }
        return 0;

    } else {
        throw "Not the same types!";
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
var array1 = ["IJ", "GH", "EF", "CD", "AB"];
var array2 = ["abc", "a", "ab", "a", "abc", "ab"];
var array3 = ["ij", "GH", "ef", "CD", "ab"];

console.log("Unsorted A1: " + array1);
console.log("Sorted A1:   " + array1.sort(compare));
console.log();

console.log("Unsorted A2: " + array2);
console.log("Sorted A2:   " + array2.sort(compareLen));
console.log();

console.log("Unsorted A3: " + array3);
console.log("Sorted A3:   " + array3.sort(compare));
console.log();

console.log("Unsorted A3: " + array3);
console.log("Sorted A3:   " + array3.sort(compareIgnoreCase));
console.log();