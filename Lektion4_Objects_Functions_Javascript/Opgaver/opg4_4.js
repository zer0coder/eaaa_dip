var num = [9, 8, 7, 6, 5, 4, 3, 2, 1];
/////////////////////////////////////////////////////////////////////////////////
function max() {
    var max = 0;
    for(var i = 0; i<this.length; i++) {
        if(max < this[i]) {
            max = this[i];
        }
    }
    return max;
}
/////////////////////////////////////////////////////////////////////////////////
function contains(element) {
    for(var i = 0; i<this.length; i++) {
        if(this[i] === element) {
            return true;
        }
    }
    return false;
}
/////////////////////////////////////////////////////////////////////////////////
function sum() {
    var total = 0;
    for(var i = 0; i<this.length; i++) {
        if(typeof this[i] === 'number') {
            total += this[i];
        }
    }
    return total;
}
/////////////////////////////////////////////////////////////////////////////////
console.log(num);
// add function properties
num.max = max;
num.contains = contains;
num.sum = sum;
// check
console.log(num);
console.log();
///// test functions
console.log("Max: " + num.max());
console.log("Contains: " + num.contains(3));
console.log("Sum: " + num.sum());