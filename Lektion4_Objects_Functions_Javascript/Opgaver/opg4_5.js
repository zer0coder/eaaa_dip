var binaryTree = {
    value: undefined,
    left: undefined,
    right: undefined,

    put: function(value) {
        var root = this;

        if(root.value === undefined) {
            root.value = value;

            root.left = { value: undefined, left: undefined, right: undefined };
            root.right = { value: undefined, left: undefined, right: undefined };

        } else {
            if(value < root.value) {
                put(root.left, value);
            } else {
                put(root.right, value);
            }
        }

        function put(node, value) {
            if(node.value === undefined) {
                node.value = value;
                node.left = { value: undefined, left: undefined, right: undefined };
                node.right = { value: undefined, left: undefined, right: undefined };
            } else {
                if(value < node.value) {
                    put(node.left, value);
                } else {
                    put(node.right, value);
                }
            }
        }

    },

    get: function(value) {
        var root = this;

        if(root.value === value) {
            return value;
        } else {
            if(value < root.value) {
                return get(root.left, value);
            } else {
                return get(root.right, value);
            }
        }

        function get(node, value) {
            if(node !== undefined) {
                if (node.value === value) {
                    return value;
                } else {
                    if (value < node.value) {
                        return get(node.left, value);
                    } else {
                        return get(node.right, value);
                    }
                }
            } else {
                return -1;
            }
        }
    },

    print: function() {
        var root = this;
        if(root.value !== undefined) {
            if (root.left.value !== undefined) {
                inorder(root.left);
            }
            console.log(root.value);
            if (root.right.value !== undefined) {
                inorder(root.right);
            }
        } else {
            console.log("Empty!");
        }

        function inorder(node) {
            if (node.left.value !== undefined) {
                inorder(node.left);
            }
            console.log(node.value);
            if (node.right.value !== undefined) {
                inorder(node.right);
            }
        }
    }
};

console.log("\nTree status: ");
binaryTree.print(); // inorder printing
// console.log(binaryTree); // debug purpose
binaryTree.put(10); // ROOT
// console.log(binaryTree);

// left sub tree insertions
binaryTree.put(5);
binaryTree.put(9);
binaryTree.put(7);
binaryTree.put(1);
binaryTree.put(4);
binaryTree.put(2);
binaryTree.put(3);
binaryTree.put(6);
// right sub tree insertions
binaryTree.put(17);
binaryTree.put(11);
binaryTree.put(16);
binaryTree.put(15);
binaryTree.put(18);
binaryTree.put(14);
binaryTree.put(19);
binaryTree.put(12);
binaryTree.put(13);
binaryTree.put(20);

// get test
console.log("\n----GET TESTING----");
console.log("Get 6:  " + binaryTree.get(6) + " (6)");
console.log("Get 21: " + binaryTree.get(21) + " (-1)");

console.log("\nPrint binary-tree (inorder printing)");
binaryTree.print();
console.log("\nInternal Structure check...");
console.log(binaryTree);
