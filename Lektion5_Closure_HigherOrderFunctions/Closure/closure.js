var x = "global";
function f() {
    console.log(x); // => undefined
    var x = "local";
    console.log(x); // => local
}
f();
console.log(x); // => global

var y = "global";
function outer() {
    var y = "local";
    function inner(){return y;}
    return inner();
}
console.log(outer()); // => local

var z = "global";
function outer2() {
    var z = "local";
    function inner(){return z;}
    return inner;
}
var inner = outer2();
console.log(inner()); // => local
console.log(outer2()()); // => local

var next = function() {
    var n = 1;
    return function(){return n++;};
}();
console.log(next()); // => 1
console.log(next()); // => 2

function counter(n) {
    n = n || 0;
    return {
        next: function(){return ++n;},
        reset: function(){n = 0;}
    };
}
var c1 = counter(9);
console.log(c1.next()); // => 10
var c2 = counter();
console.log(c2.next()); // => 1
console.log(c1.next()); // => 11
c1.reset();
console.log(c1.next()); // => 1

var h;
function g() {
    var x = "before";
    h = f; // f is hoisted
    function f(){return x;}
    x = "after";
}
g();
console.log(h()); // => after

function wait(p) {
    function callback(){console.log(p);}
    setTimeout(callback, 2000);
    console.log("wait ..."); // => wait ...
}
wait(112);
// => 112

