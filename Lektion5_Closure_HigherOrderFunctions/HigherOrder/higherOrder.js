function multiplier(factor) {
    return function(number) {return number*factor;};
}
var twice = multiplier(2);
console.log(twice(4)); // => 8

function log(f){
    return function(x) {
        var y = f(x);
        console.log('log: ' + x + ' -> ' + y); // => 2 -> 4
        return y;
    };
}
console.log(log(twice)(2)); // => 4

function unless(test, then) {
    if (!test) then();
}
function repeat(times, body) {
    for (var i = 0; i < times; i++) body(i);
}

repeat(3, function(n) {
    unless(n % 2, function() {
        console.log(n, "is even");
    });
});
// => 0 is even
// => 2 is even

var data = [1,2,3,4,5];
var sum = 0;
data.forEach(function(v){sum += v;});
console.log(sum); // => 15

data.forEach(function(v, i, a){a[i] = v + 1;});
console.log(data); // => [2,3,4,5,6]

var a = [1,2,3,4,5];
var b = a.map(function(x){return x*x;});
var c = b.map(Math.sqrt);
console.log(b); // => [ 1, 4, 9, 16, 25 ]
console.log(c); // => [ 1, 2, 3, 4, 5 ]

var d = [1,2,3,4,5];
var e = d.filter(function(x){return x < 3;});
var f = d.filter(function(x,i){return i%2==0;});
console.log(e); // => [ 1, 2 ]
console.log(f); // => [ 1, 3, 5 ]

delete d[2];
console.log(d); // => [ 1, 2, , 4, 5 ]
var g = d.filter(function(){return true;});
console.log(g); // => [ 1, 2, 4, 5 ]

var h = [1,2,3,4,5];
var i = h.every(function(x){return x < 10;});
var j = h.every(function(x){return x % 2 === 0;});
var k = h.some(function(x){return x % 2 === 0;});
console.log(i, j , k); // => true false true

var l = [1,2,3,4,5];
var m = l.reduce(function(x,y){return x+y;});
var n = l.reduce(function(x,y){return x*y;});
var o = l.reduce(function(x,y){return (x>y)?x:y;});
console.log(m, n, o); // => 15 120 5

var p = [1,2,3,4,5];
var q = function(x){return x%2;};
var r = function(x){return x*x;};
var s = function(x,y){return x+y;};
var t = p.filter(q).map(r).reduce(s);
console.log(t); // => 35

