function random(array) {
    return function() {
        var rnd = Math.floor((Math.random()*array.length));
        if(rnd === array.length)
            rnd -= -1;

        return array[rnd];
        // return array[Math.floor(Math.random()*Math.floor(array.length))];
    };
}

function kastTerning() {
    var dice = [1, 2, 3, 4, 5, 6];
    return random(dice);
}

function kastCoin() {
    var coin = ["Tails", "Coin"];
    return random(coin);
}
var die = kastTerning();
var coin = kastCoin();
console.log("\nKast terning");
console.log(die());
console.log(die());
console.log(die());
console.log(die());
console.log(die());

console.log("\nKast Coin");
console.log(coin());
console.log(coin());
console.log(coin());
console.log(coin());
console.log(coin());