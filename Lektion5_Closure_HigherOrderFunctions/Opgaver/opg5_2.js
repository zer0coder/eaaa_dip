function compare(s1, s2) {
    if (typeof s1 === "string" && typeof s2 === "string") {

        for(var i = 0; i<s1.length; i++) {
            for(var j = 0; j<s2.length; j++) {
                if(s1[i] < s2[j])
                    return -1;
                else if (s1[i] > s2[j])
                    return 1;
            }
        }
        return 0;

    } else {
        throw "Not the same types!";
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////
function compareLen(s1, s2) {
    if (typeof s1 === "string" && typeof s2 === "string") {

        return s1.length - s2.length;

    } else {
        throw "Not the same types!";
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////
function compareIgnoreCase(s1, s2) {
    if (typeof s1 === "string" && typeof s2 === "string") {

        var t1 = s1.toUpperCase();
        var t2 = s2.toUpperCase();

        for(var i = 0; i<t1.length; i++) {
            for(var j = 0; j<t2.length; j++) {
                if(t1[i] < t2[j])
                    return -1;
                else if (t1[i] > t2[j])
                    return 1;
            }
        }
        return 0;

    } else {
        throw "Not the same types!";
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////
var array1 = ["IJ", "GH", "EF", "CD", "AB"];
var array2 = ["abc", "a", "ab", "a", "abc", "ab"];
var array3 = ["ij", "GH", "ef", "CD", "ab"];
var array4 = ["aaa", "aAa", "Ab", "B", "AbC", "aB"];
////////////////////////////////////////////////////////////////////////////////////////////////

function compareSort(compare) {
    return function sort(array) {
        return array.sort(compare);
    }
}

var lenSort = compareSort(compareLen);
var ignoreCaseSort = compareSort(compareIgnoreCase);
var regularSort = compareSort(compare);

console.log(lenSort(array1));
console.log(lenSort(array2));
console.log(lenSort(array3));
console.log();
console.log(ignoreCaseSort(array1));
console.log(ignoreCaseSort(array2));
console.log(ignoreCaseSort(array3));
console.log();
console.log(regularSort(array4));
