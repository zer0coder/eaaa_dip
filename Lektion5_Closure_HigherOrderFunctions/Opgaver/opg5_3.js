function observers() {
    console.log("notified");
}

function subject() {
    var observers = [];

    return {
        registerObserver: function(observer) {
            observers.push(observer);
        },

        notifyObservers: function() {
            for(var i = 0; i<observers.length; i++) {
                observers[i]();
            }
        }
    }
}

var testSubject = subject();

testSubject.notifyObservers();
console.log();
testSubject.registerObserver(observers);
testSubject.registerObserver(observers);
testSubject.registerObserver(observers);
testSubject.notifyObservers();
console.log();
testSubject.registerObserver(observers);
testSubject.registerObserver(observers);
testSubject.notifyObservers();

