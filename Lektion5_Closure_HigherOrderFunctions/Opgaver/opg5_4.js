var personer = [];
// insert
personer[0] = {navn: "Anders", email: "foo@gmail.com", tlf: "+45 88888888"};
personer[1] = {navn: "Bo", email: "bar@gmail.com", tlf: "+60 12345678"};
personer[2] = {navn: "Grene", email: "test@gmail.com", tlf: "+80 01010101"};
personer[3] = {navn: "Francisco", email: "fgluver@gmail.com", tlf: "+25 54545454"};
personer[4] = {navn: "Line", email: "hots@gmail.com", tlf: "+45 90909090"};
personer[5] = {navn: "Martin", email: "mtin@gmail.com", tlf: "+77 11111111"};
////////////////////////////////////////////////////////////////////////////////////////////////////////////
var names = "";
// var names;

personer.forEach(function (value) {

    names += value.navn + ", ";
});

// names = personer.map(function (value) {
//     return value;
// }).sort().reduce(function (x, y) {
//     return x + ", " + y;
// });

console.log("");
console.log(names);
console.log("");
////////////////////////////////////////////////////////////////////////////////////////////////////////////
var dk_phone = personer.filter(function (value) {
    var split = value.tlf.split(" ");
    if(split[0] === "+45") {
        return {
            navn: value.navn,
            tlf: value.tlf
        };
    }
});
console.log("=====DK TLF=====");
console.log(dk_phone);
console.log("");
////////////////////////////////////////////////////////////////////////////////////////////////////////////
console.log("=====PERSONER=====");
// console.log(personer);
//
// personer = personer.map(function (value, index) {
//     return {id: index, navn: value.navn, email: value.email, tlf: value.tlf};
// });

personer.forEach(function (value, index) {
    value.id = index;
});

console.log(personer);
////////////////////////////////////////////////////////////////////////////////////////////////////////////