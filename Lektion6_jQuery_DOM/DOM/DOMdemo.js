var h1 = document.querySelector("h1");
var newP = document.createElement("p");
newP.innerHTML = "New paragraph";
h1.parentElement.insertBefore(newP, h1);

var appendP = document.createElement("p");
appendP.innerHTML = "Appended paragraph to body";
document.body.appendChild(appendP);

var newSecondP = document.createElement("p");
newSecondP.innerHTML = "Second paragraph";
var p = document.querySelectorAll("p")[1];
document.body.insertBefore(newSecondP, p);

ps = document.querySelectorAll("p");
for (var i = 0; i < ps.length; i++) {
    ps[i].style.backgroundColor = "lightgrey";
    ps[i].innerHTML = "" + (i+1) +": " + ps[i].innerHTML;
}

