// Tilføj kode her!

/// OPG 6.1
var ps = document.querySelectorAll("p");

for(var i = 0; i<ps.length; i++) {
    ps[i].className = "red";
}

/// OPG 6.2
var trs = document.querySelectorAll("tr");

for(var i = 0; i<trs.length; i++) {
    if(i % 2 === 1)
        trs[i].className = "grey";
}

/// OPG 6.3
var hs = document.querySelectorAll("h1");

for(var i = 0; i<hs.length; i++) {
    var pss = hs[i].nextElementSibling;
    while(pss.nodeName !== "P") {
        pss = pss.nextElementSibling;
    }

    var h2s = document.createElement("h2");
    h2s.innerHTML = pss.innerHTML;
    h2s.className = pss.className;

    hs[i].parentElement.replaceChild(h2s, pss);
}
/// OPG 6.4
for(var i = 0; i<hs.length; i++) {
    var a = document.createElement("a");
    a.href = "#";
    a.innerHTML = "Link [" + i + "]: " + hs[i].innerHTML;

    hs[i].parentElement.insertBefore(a, hs[0]);
}