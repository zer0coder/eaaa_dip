
var newP = $("<p>New paragraph</p>");
$("h1").before(newP);

$("body").append("<p>Appended paragraph to body</p>");

$("p").eq(1).before("<p>Second paragraph</p>");

$("p")
    .css("backgroundColor", "lightgray")
    .each(function (i) {
        $(this).prepend("" + (i + 1) + ": ");
    });

