var url = 'http://jsonplaceholder.typicode.com/users';
$.getJSON(url, function(users){
    for(var i in users)
        console.log(users[i].name);
    }
);

$.getJSON(url)
    .then(function (users) {
            for (var i in users)
                console.log(users[i].name);
        }
    )
    .catch(function() {
        console.log('Kan ikke hente JSON');
    });

