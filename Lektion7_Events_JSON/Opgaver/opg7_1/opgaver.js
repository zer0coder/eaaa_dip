// Tilføj kode her!

$(document).ready(function () {

    // Opgave 7.1
    var toggleText = $("<button>Skjul liste</button>").attr("id", "toggleText");
    toggleText.css({"margin": "0px 0px 10px 0px"});

    $("table").before(toggleText);

    $("#toggleText").on("click", function() {
        $("table").slideToggle();
        var text = $(this).text();
        if(text === "Vis liste") {
            $(this).text("Skjul Liste");
        } else {
            $(this).text("Vis liste");
        }
    });

/// OPG 6.1
    $("p").addClass("red");

/// OPG 6.2

    $("tr:odd").addClass("grey");

/// OPG 6.3

    // $("h1").next().wrap("<h2>").contents().unwrap(); // korrekt
    $("h1").next().contents().unwrap().wrap("<h2>"); // korrekt

/// OPG 6.4

     $("h1").each(function (i) {
         $(this).attr("id", 1 + i);
     });

     for(var i = 0; i < $("h1").length; i++) {
         var text = $("#" + (1 + i)).html();
         var link = $("<a href=#" + (i+1) + ">" + "<p>" + text + "</p></a>");
         $("h1:first").before(link);
     }

/// OPG 6.5

    var personer = [];
// insert
    personer[0] = {navn: "Anders", email: "foo@gmail.com", tlf: "+45 88888888"};
    personer[1] = {navn: "Bo", email: "bar@gmail.com", tlf: "+60 12345678"};
    personer[2] = {navn: "Grene", email: "test@gmail.com", tlf: "+80 01010101"};
    personer[3] = {navn: "Francisco", email: "fgluver@gmail.com", tlf: "+25 54545454"};
    personer[4] = {navn: "Line", email: "hots@gmail.com", tlf: "+45 90909090"};
    personer[5] = {navn: "Martinæ", email: "mtin@gmail.com", tlf: "+77 11111111"};


    $("table").empty();
    for(var i = 0; i < personer.length; i++) {
        $("table").append("<tr>"+ "<td>" + personer[i].navn + "</td>" + "<td>" + personer[i].email + "</td>" + "<td>" + personer[i].tlf + "</td></tr>")
    }
});