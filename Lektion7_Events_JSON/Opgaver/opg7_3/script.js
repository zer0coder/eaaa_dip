$(document).ready(function() {
    var url_source = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_day.geojson";
    $.getJSON(url_source).then(function (entry) {

        var data = entry.features;
        var table = $("#jsonTable");

        for(var entry in data) {
            var prop = data[entry].properties;

            table.append("<tr>"+ "<td>" + prop.place + "</td><td>"
                + prop.mag + "</td><td>" + (new Date(prop.time)) + "</td></tr>")
        }

    }).catch(function () {
        console.log("Error");
    })
});