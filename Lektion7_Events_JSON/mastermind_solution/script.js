"use strict";

var activeRow = 0;
var secret = createSecret();

function createSecret() {
    var colors = ["red", "blue", "yellow", "green", "purple", "orange"];
    var res = [];
    for (var i = 0; i < 4; i++) {
        res.push(colors[Math.round(Math.random() * 5)]);
    }
    return res;
}

function handleRow(row, column) {
    //console.log(row + " " + column);
    if (row != activeRow) {
        console.log('Wrong row!')
    } else if (mm.getSelectedColor()) {
        var color = mm.getSelectedColor();
        mm.addPin(color, row, column);
        if (rowFull(row)) {
            activeRow++;
            addStatus(row);
        }
    }
}

function rowFull(row) {
    var colors = mm.getRow(row);
    for (var i = 0; i < 4; i++) {
        var e = colors[i]
        if (!e) {
            return false;
        }
    }
    return true;
}

function addStatus(row) {
    var colors = mm.getRow(row);
    var secretCopy = secret.slice();
    var cScore = 0;
    var pScore = 0;
    var res = "";
    for (var i = 0; i < 4; i++) {
        if (colors[i] == secretCopy[i]) {
            pScore++;
            colors[i] = null;
            secretCopy[i] = null;
        }
    }

    for (var i = 0; i < 4; i++) {
        for (var j = 0; j < 4; j++) {
            if (colors[i] != null && colors[i] == secretCopy[j]) {
                cScore++;
                colors[i] = null;
                secretCopy[j] = null;
            }
        }
    }

    mm.addStatus(pScore, cScore, row);

    if (pScore == 4) {
        mm.setMessage("Congratulations! You guessed the secret! Reload the page to play again.");
    } else if (activeRow == 10) {
        mm.setMessage("I'm sorry, you're lost the game. The secret was: " + secret + ". " +
        "Reload the page to try again.");
    }
}

$(document).ready(function () {
    var secrets = $("<p>" + secret + "</p>");
    $("#showSecret").css({"margin": "10px 0px 0px 0px"});
    $("#showSecret").after(secrets);
    secrets.slideUp();
    $("#showSecret").on("click", function () {
        secrets.slideToggle();
    });
});