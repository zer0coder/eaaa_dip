///////////////////////////////////////////////////////////////////////////////////////////
var express = require("express");
var morgan = require("morgan");
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var app = express();
///////////////////////////////////////////////////////////////////////////////////////////
app.use(morgan("tiny"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static("public"));

var dbuser = "";
var dbpassword = "";

var databaseURL = "mongodb://localhost/worker-test";
// var databaseURL = "mongodb://" + dbuser + ":" + dbpassword + "@ds033255.mlab.com:33255/opgave15";

mongoose.Promise = global.Promise;
mongoose.connect(databaseURL, {
    useMongoClient: true
});
///////////////////////////////////////////////////////////////////////////////////////////
var Schema = mongoose.Schema;
var lejelighed = new Schema({
    id: String,
    name: String,
    forening: String,
    temperatur: Number,
    elektricitet: Number,
    water: Number
});
lejelighed.methods.identify = function () {
    console.log("(" + this.id + ") " + this.name + " - " + this.forening);
};

// var Lejelighed = require("./model/Lejelighed");
var Lejelighed = mongoose.model('leje', lejelighed);
///////////////////////////////////////////////////////////////////////////////////////////
var port = 8080;
///////////////////////////////////////////////////////////////////////////////////////////
app.get("/worker-test", function (req, res) {

    Lejelighed.find(function (err, messages) {
        if (err) {
            res.send(err);
        } else {
            res.json(messages);
        }
    })
})
.post("/worker-test", function (req, res) {

    var lejelighed = new Lejelighed({
        id: req.body.id,
        name: req.body.name,
        forening: req.body.forening,
        temperatur: req.body.temperatur,
        elektricitet: req.body.elektricitet,
        water: req.body.water
    });

    if(lejelighed !== undefined) {

        console.log(lejelighed);

        lejelighed.save(function (err) {
            if(err) {
                res.status(500);
                res.send(err);
            } else {
                res.status(201).send("Lejeligheds data sendt...");
            }
        })

    } else {
        res.status(400).send("Bad Request - lejelighed data undefined.")
    }
});

app.listen(port);
console.log("Server running on port: " + port);