var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var lejelighed = new Schema({
    id: String,
    name: String,
    forening: String,
    temperatur: Number,
    elektricitet: Number,
    water: Number
});

lejelighed.methods.identify = function () {
    console.log("(" + this.id + ") " + this.name + " - " + this.forening);
};

module.exports = mongoose.model("Lejelighed", lejelighed);