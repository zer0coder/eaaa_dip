var name;
var id;
var forening;

self.addEventListener("message", function (ev) {

    var args = ev.data.args;

    id       = args[0];
    name     = args[1];
    forening = args[2];

    console.log(args);

    self.setTimeout(doSomething, 5000);

}, false);


function doSomething() {

    var temperature = Math.floor((Math.random() * 100) + 1);
    var electricity = Math.floor((Math.random() * 100) + 1);
    var water = Math.floor((Math.random() * 100) + 1);

    var data = {
        "id": id,
        "name": name,
        "forening": forening,
        "temperatur": temperature,
        "elektricitet": electricity,
        "water": water
    };

    self.postMessage(data);
    setTimeout(doSomething, 3000);
}
