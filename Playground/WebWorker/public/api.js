$(function () {

    /////////////////////////////////////////////////////////////////////////////////////

    if(typeof (Worker) !== "undefined") {
        console.log("OK");
    } else {
        alert("Your browser does not support Web Workers!");
    }

    /////////////////////////////////////////////////////////////////////////////////////
    var url = "http://localhost:8080/worker-test";

    var workerArray = [];
    var worker;
    var consoleBox = $("#console");
    var workerList = $("#workerList");

    /////////////////////////////////////////////////////////////////////////////////////

    $("#startWorker").on("click", function (event) {
        event.preventDefault();

        var id = $("#value1").val();
        var name = $("#value2").val();
        var forening = $("#value3").val();

        worker = new Worker("WorkerExamp.js");

        worker.onmessage = function (event) {
            var str = JSON.stringify(event.data);
            consoleBox.append(str);
            PostToDatabase(event.data);
        };

        worker.postMessage({ "args": [id, name, forening] }); // Start
        addToWorkerList(id, name, forening);
        workerArray.push(worker);
    });

    /////////////////////////////////////////////////////////////////////////////////////

    function addToWorkerList(id,name,forening) {
        var arrayNum = workerArray.length-1;

        var button = document.createElement("button");
        button.className = "btn btn-danger";
        button.id = "worker"+arrayNum;
        button.innerHTML = "Terminate";
        button.onclick = DynamicStopButton;

        var li = document.createElement("li");
        li.className = "list-group-item";
        li.innerHTML = "ID: " + id
        + ", NAVN: " + name
        + ", FORENING: " + forening;
        li.appendChild(button);

        workerList.append(li);
    }

    function DynamicStopButton() {
        var me = $(this);
        var id = me.attr("id").split("-")[1] - 1;
        console.log(id);

        worker = workerArray[id];
        console.log(worker);

        worker.terminate();
        console.log("T-" + id + " terminated...");

        me.parent().remove();
    }

    function PostToDatabase(data) {
        console.log(data);
        $.post(url, {

            id: data.id,
            name: data.name,
            forening: data.forening,
            temperatur: data.temperatur,
            elektricitet: data.elektricitet,
            water: data.water

        }).then(function (response) {
            console.log(response);
        }).catch(function (reason) {
            console.log("Indsend fejl: \n" + JSON.stringify(reason));
        });
    }

    /////////////////////////////////////////////////////////////////////////////////////

    $.get(url, function (data) {

        var arrItems = [];      // THE ARRAY TO STORE JSON ITEMS.
        $.each(data, function (index, value) {
            console.log(value);
            arrItems.push(value);       // PUSH THE VALUES INSIDE THE ARRAY.
        });

        // EXTRACT VALUE FOR TABLE HEADER.
        var col = [];
        for (var i = 0; i < arrItems.length; i++) {
            for (var key in arrItems[i]) {
                if (col.indexOf(key) === -1) {
                    col.push(key);
                }
            }
        }

        // CREATE DYNAMIC TABLE.
        var table = document.createElement("table");
        table.className = "table table-striped";

        // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

        var tr = table.insertRow(-1);                   // TABLE ROW.

        for (var i = 0; i < col.length; i++) {
            var th = document.createElement("th");      // TABLE HEADER.
            th.innerHTML = col[i];
            tr.appendChild(th);
        }

        // ADD JSON DATA TO THE TABLE AS ROWS.
        for (var i = 0; i < arrItems.length; i++) {

            tr = table.insertRow(-1);

            for (var j = 0; j < col.length; j++) {
                var tabCell = tr.insertCell(-1);
                tabCell.innerHTML = arrItems[i][col[j]];
            }
        }

        // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
        var divContainer = document.getElementById("showData");
        divContainer.innerHTML = "";
        divContainer.appendChild(table);
    });

});